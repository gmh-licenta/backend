<?php

use Api\UserProfile\UserProfile;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

class UsersProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'users_profiles';

        Schema::disableForeignKeyConstraints();
        DB::table($table)->truncate();
        Schema::enableForeignKeyConstraints();

        $profilePictures = Storage::disk('profile_pictures')->allFiles();
        Storage::disk('profile_pictures')->delete($profilePictures);

        UserProfile::create([
            'user_id' => 1,
            'department_id' => 1,
            'position_id' => 1,
            'phone' => '',
            'birthday' => Carbon::now()->toDateTimeString(),
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
        UserProfile::create([
            'user_id' => 2,
            'department_id' => 3,
            'position_id' => 3,
            'phone' => '+40732345678',
            'birthday' => Carbon::now()->subYears(2)->startOfDay()->toDateTimeString(),
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);

        UserProfile::create([
            'user_id' => 3,
            'department_id' => 5,
            'position_id' => 5,
            'phone' => '+40734456789',
            'birthday' => Carbon::now()->subYears(4)->startOfDay()->toDateTimeString(),
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);

        UserProfile::create([
            'user_id' => 4,
            'department_id' => 5,
            'position_id' => 5,
            'phone' => '+40735456789',
            'birthday' => Carbon::now()->subYears(4)->startOfDay()->toDateTimeString(),
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);

        UserProfile::create([
            'user_id' => 5,
            'department_id' => 5,
            'position_id' => 5,
            'phone' => '+40736456789',
            'birthday' => Carbon::now()->subYears(4)->startOfDay()->toDateTimeString(),
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);

        UserProfile::create([
            'user_id' => 6,
            'department_id' => 5,
            'position_id' => 5,
            'phone' => '+40737456789',
            'birthday' => Carbon::now()->subYears(4)->startOfDay()->toDateTimeString(),
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
    }
}
