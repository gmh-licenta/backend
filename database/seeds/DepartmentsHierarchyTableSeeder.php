<?php

use Api\Department\Department;
use Api\Department\Hierarchy\DepartmentsHierarchy;
use Api\UserProfile\UserProfile;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DepartmentsHierarchyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'departments_hierarchy';

        Schema::disableForeignKeyConstraints();
        DB::table($table)->truncate();
        Schema::enableForeignKeyConstraints();

        $users = UserProfile::all();

        $indexes = [];
        foreach ($users as $user)
        {
            if (in_array($user->department->id, $indexes))
            {
                continue;
            }

            $index = count($indexes) + 1;

            DepartmentsHierarchy::create([
                'department_id' => $user->department->id,
                'index' => $index,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]);

            $indexes[] = $user->department->id;
        }

//        $departments = Department::all();
//
//        $index = 1;
//        foreach ($departments as $department)
//        {
//            DepartmentsHierarchy::create([
//                'department_id' => $department->id,
//                'index' => $index,
//                'created_at' => Carbon::now()->toDateTimeString(),
//                'updated_at' => Carbon::now()->toDateTimeString(),
//            ]);
//
//            $index++;
//        }
    }
}
