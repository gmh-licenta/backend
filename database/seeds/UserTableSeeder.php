<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        User::truncate();

        Schema::enableForeignKeyConstraints();


//        DB::table('users')->insert([
//            'name' => 'Admin',
//            'email' => 'admin@example.com',
//            'password' => Hash::make('asd123'),
//            'created_at' => Carbon::now()->toDateTimeString(),
//            'updated_at' => Carbon::now()->toDateTimeString(),
//        ]);

        User::create([
            'name' => 'Super Admin',
            'email' => 'sa@example.com',
            'password' => Hash::make('asd123'),
        ]);

        User::create([
            'name' => 'Admin',
            'email' => 'admin@example.com',
            'password' => Hash::make('asd123'),
        ]);

        User::create([
            'name' => 'Employee',
            'email' => 'emp@example.com',
            'password' => Hash::make('asd123'),
        ]);

        User::create([
            'name' => 'Employee 2',
            'email' => 'emp2@example.com',
            'password' => Hash::make('asd123'),
        ]);

        User::create([
            'name' => 'Employee 3',
            'email' => 'emp3@example.com',
            'password' => Hash::make('asd123'),
        ]);

        User::create([
            'name' => 'Employee 4',
            'email' => 'emp4@example.com',
            'password' => Hash::make('asd123'),
        ]);
    }
}
