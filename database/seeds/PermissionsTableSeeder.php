<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Permission::truncate();
        Schema::enableForeignKeyConstraints();

        $permissions = Config::get('api_roles_permissions.permissions');

        /*
         * Permissions to launch or perform actions
         * */

        foreach ($permissions['action'] as $permission)
        {
            Permission::create([
                'name' => $permission,
                'guard_name' => 'api',
            ]);
        }

        /*
         * Permissions to view default app sections
         * */

        foreach ($permissions['view'] as $permission)
        {
//            $viewPermission = 'view.' . $permission;
            Permission::create([
                'name' => $permission,
                'guard_name' => 'api',
            ]);
        }
    }
}
