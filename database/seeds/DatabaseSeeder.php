<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolesTableSeeder::class,
            PermissionsTableSeeder::class,
            RoleHasPermissionsTableSeeder::class,
            UserTableSeeder::class,
            ModelHasRolesTableSeeder::class,
            DepartmentsTableSeeder::class,
            PositionsTableSeeder::class,
            UsersProfilesTableSeeder::class,
            ConfigTableSeeder::class,
            DepartmentsHierarchyTableSeeder::class,
            PositionsHierarchyTableSeeder::class,
        ]);
    }
}
