<?php

use Api\Position\Hierarchy\PositionsHierarchy;
use Api\UserProfile\UserProfile;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class PositionsHierarchyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'positions_hierarchy';

        Schema::disableForeignKeyConstraints();
        DB::table($table)->truncate();
        Schema::enableForeignKeyConstraints();

        $users = UserProfile::all();

        $indexes = [];
        foreach ($users as $user)
        {
            if (!array_key_exists($user->department->id, $indexes))
            {
                $indexes[$user->department->id] = [];
            }

            $index = count($indexes[$user->department->id]) + 1;

            if (in_array($user->position->id, $indexes[$user->department->id]))
            {
                continue;
            }

            PositionsHierarchy::create([
                'department_id' => $user->department->id,
                'position_id' => $user->position->id,
                'index' => $index,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]);

            $indexes[$user->department->id][] = $user->position->id;
        }
    }
}
