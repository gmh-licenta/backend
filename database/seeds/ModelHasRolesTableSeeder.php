<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ModelHasRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'model_has_roles';

        Schema::disableForeignKeyConstraints();
        DB::table($table)->truncate();
        Schema::enableForeignKeyConstraints();

        $superAdminUser = User::where('email', 'sa@example.com')->first();
        $superAdminUser->assignRole('SuperAdmin');

        $adminUser = User::where('email', 'admin@example.com')->first();
        $adminUser->assignRole('Admin');

        $empUser = User::where('email', 'emp@example.com')->first();
        $empUser->assignRole('Employee');

        $empUser = User::where('email', 'emp2@example.com')->first();
        $empUser->assignRole('Employee');

        $empUser = User::where('email', 'emp3@example.com')->first();
        $empUser->assignRole('Employee');

        $empUser = User::where('email', 'emp4@example.com')->first();
        $empUser->assignRole('Employee');
    }
}
