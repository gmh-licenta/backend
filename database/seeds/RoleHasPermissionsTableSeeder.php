<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleHasPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'role_has_permissions';

        Schema::disableForeignKeyConstraints();
        DB::table($table)->truncate();
        Schema::enableForeignKeyConstraints();

        $rolesPermissions = Config::get('api_roles_permissions.roles_permissions');

        $_permissions = Permission::all();
        $_roles = Role::all();

        foreach ($rolesPermissions as $role => $rolePermissions)
        {
            $_role = $_roles->where('name', $role)->first();

            foreach ($rolePermissions as $permissionType => $permissions)
            {
                foreach ($permissions as $permission)
                {
                    $_permission = $_permissions->where('name', $permission)->first();

                    $_role->givePermissionTo($_permission);
                }
            }
        }
    }
}
