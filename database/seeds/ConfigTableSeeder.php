<?php

use Api\Config\Config;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config as ConfigFacade;

class ConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Config::truncate();

//        $now = Carbon::now();

        $defaultConfig = ConfigFacade::get('api_config');

        foreach ($defaultConfig as $entry)
        {
            Config::create([
                'option' => $entry['option'],
                'value' => $entry['value'],
            ]);
        }

//        Config::create([
//            'option' => 'initialized',
//            'value' => 'false',
//        ]);
//
//
////        $tokenLifetime = $now->copy()->addMinutes(30)->diffInSeconds($now);
//        Config::create([
//            'option' => 'token_lifetime',
////            'value' => $tokenLifetime,
//            'value' => $defaultConfig[1]['value'],
//        ]);
//
//        Config::create([
//            'option' => 'country',
////            'value' => 'RO',
//            'value' => $defaultConfig[2]['value'],
//        ]);
    }
}
