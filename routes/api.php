<?php

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use Api\Config\Config as ApiConfig;
use Illuminate\Http\Response;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
    [
        'namespace' => 'Passport',
        'prefix' => 'oauth',
        'as' => 'passport.',
    ],
    function () {
        Route::group(
            [
                'namespace' => 'Auth',
                'prefix' => 'auth',
                'as' => 'auth.',
            ],
            function () {
                Route::post('login', 'LoginController@index')->name('login');
            }
        );

        Route::group(
            [
                'namespace' => 'Client',
                'prefix' => 'client',
                'as' => 'client.',
            ],
            function () {
                Route::post('/', 'ClientController@store')->name('store');
                Route::post('find-or-fail', 'ClientController@findOrFail')->name('find_or_fail');
            }
        );
    }
);

Route::group(
    [
        'namespace' => 'Config',
        'prefix' => 'config',
        'as' => 'config.',
    ],
    function () {
        Route::get('initialized', 'ConfigController@isInitialized')->name('is_initialized');
    }
);

Route::group(
    [
        'middleware' => [
            'auth:api',
        ],
    ],
    function () {

        Route::group(
            [
                'middleware' => [
                    'role:SuperAdmin'
                ]
            ],
            function () {
                Route::group(
                    [
                        'namespace' => 'Config',
                        'prefix' => 'config',
                        'as' => 'config.',
                    ],
                    function () {
                        Route::get('get', 'ConfigController@get')->name('get');
                        Route::get('get-default', 'ConfigController@getDefault')->name('get_default');
                        Route::get('get-supported-countries', 'ConfigController@getSupportedCountries')
                            ->name('get_supported_countries');
                        Route::post('update', 'ConfigController@update')->name('update');
                    }
                );
                Route::group(
                    [
                        'namespace' => 'LegalHoliday',
                        'prefix' => 'legal-holiday',
                        'as' => 'legal_holiday.',
                    ],
                    function () {
                        Route::get('initialize-default', 'LegalHolidayController@initializeDefault')
                            ->name('initialize_default');
                        Route::post('delete', 'LegalHolidayController@delete')
                            ->name('delete');
                    }
                );
                Route::group(
                    [
                        'namespace' => 'AvailablePaidLeaveDays',
                        'prefix' => 'available-paid-leave-days',
                        'as' => 'available_paid_leave_days.',
                    ],
                    function () {
                        Route::post('create', 'AvailablePaidLeaveDaysController@create')
                            ->name('create');
                        Route::post('update', 'AvailablePaidLeaveDaysController@update')
                            ->name('update');
                    }
                );
                Route::group(
                    [
                        'namespace' => 'Department\Hierarchy',
                        'prefix' => 'department/hierarchy',
                        'as' => 'department.hierarchy.',
                    ],
                    function () {
//                        Route::get('get', 'DepartmentsHierarchyController@get')
//                            ->name('get');
                        Route::post('create', 'DepartmentsHierarchyController@create')
                            ->name('create');
                        Route::post('update', 'DepartmentsHierarchyController@update')
                            ->name('update');
                        Route::post('delete', 'DepartmentsHierarchyController@delete')
                            ->name('delete');
                    }
                );
                Route::group(
                    [
                        'namespace' => 'Position\Hierarchy',
                        'prefix' => 'position/hierarchy',
                        'as' => 'position.hierarchy.',
                    ],
                    function () {
//                        Route::get('get', 'PositionsHierarchyController@get')
//                            ->name('get');
                        Route::post('create', 'PositionsHierarchyController@create')
                            ->name('create');
                        Route::post('update', 'PositionsHierarchyController@update')
                            ->name('update');
                        Route::post('delete', 'PositionsHierarchyController@delete')
                            ->name('delete');
                    }
                );
            }
        );

        Route::group(
            [
                'namespace' => 'Passport',
                'prefix' => 'oauth',
                'as' => 'passport.',
            ],
            function () {
                Route::group(
                    [
                        'namespace' => 'Auth',
                    ],
                    function () {
                        Route::group(
                            [
                                'prefix' => 'auth',
                                'as' => 'auth.',
                            ],
                            function () {
                                Route::post('logout', 'LogoutController@index')->name('logout');
                            }
                        );

                        Route::group(
                            [
                                'prefix' => 'token',
                                'as' => 'token.'
                            ],
                            function () {
                                Route::post('check', 'TokenController@checkToken')
                                    ->name('check');
                                Route::post('refresh', 'TokenController@refreshToken')
                                    ->name('refresh');
                            }
                        );
                    }
                );
            }
        );

        Route::group(
            [
                'namespace' => 'Dashboard',
                'prefix' => 'dashboard',
                'as' => 'dashboard.',
            ],
            function () {
                Route::get('index', 'DashboardController@index')->name('index');
            }
        );

        Route::group(
            [
                'namespace' => 'LegalHoliday',
                'prefix' => 'legal-holiday',
                'as' => 'legal_holiday.',
            ],
            function () {
                Route::get('get', 'LegalHolidayController@get')->name('get');
            }
        );

        Route::group(
            [
                'namespace' => 'Clocking',
                'prefix' => 'clocking',
                'as' => 'clocking.',
            ],
            function () {
                Route::post('clock-in', 'ClockingController@clockIn')->name('clock_in');
                Route::post('clock-out', 'ClockingController@clockOut')->name('clock_out');
                Route::get('get-current-working-day', 'ClockingController@getCurrentWorkingDayClocking')
                    ->name('get_current_working_day');
                Route::get('get', 'ClockingController@getForCurrentUser')->name('get');
            }
        );

        Route::group(
            [
                'namespace' => 'PersonalTimeOff',
            ],
            function () {
                Route::group(
                    [
                        'namespace' => 'PaidLeave',
                        'prefix' => 'paid-leave',
                        'as' => 'paid_leave.',
                    ],
                    function () {
                        Route::post('create', 'PaidLeaveController@create')
                            ->name('create');
                        Route::get('get', 'PaidLeaveController@get')
                            ->name('get');
                    }
                );

                Route::group(
                    [
                        'namespace' => 'SickLeave',
                        'prefix' => 'sick-leave',
                        'as' => 'sick_leave.',
                    ],
                    function () {
                        Route::post('create', 'SickLeaveController@create')
                            ->name('create');
                        Route::get('get', 'SickLeaveController@get')
                            ->name('get');
                    }
                );
            }
        );

        Route::group(
            [
                'namespace' => 'WorkFromHome',
                'prefix' => 'work-from-home',
                'as' => 'work_from_home.',
            ],
            function () {
                Route::post('create', 'WorkFromHomeController@create')
                    ->name('create');
                Route::get('get', 'WorkFromHomeController@get')
                    ->name('get');
            }
        );

        Route::group(
            [
                'namespace' => 'UserProfile',
                'prefix' => 'user-profile',
                'as' => 'user_profile.',
            ],
            function () {
                Route::post('update', 'UserProfileController@update')
                    ->name('update');
                Route::get('get', 'UserProfileController@getForCurrentUser')
                    ->name('get');
                Route::post('phone-number-is-taken', 'UserProfileController@phoneNumberIsTaken')
                    ->name('phone_number_is_taken');
            }
        );

        Route::group(
            [
                'namespace' => 'AvailablePaidLeaveDays',
                'prefix' => 'available-paid-leave-days',
                'as' => 'available_paid_leave_days.',
            ],
            function () {
                Route::get('get', 'AvailablePaidLeaveDaysController@get')
                    ->name('get');
            }
        );

        Route::group(
            [
                'middleware' => [
                    'has_permission',
                ],
            ],
            function () {
                Route::group(
                    [
                        'namespace' => 'Department\Hierarchy',
                        'prefix' => 'department/hierarchy',
                        'as' => 'department.hierarchy.',
                    ],
                    function () {
                        Route::get('get', 'DepartmentsHierarchyController@get')
                            ->name('get');
                    }
                );
                Route::group(
                    [
                        'namespace' => 'Position\Hierarchy',
                        'prefix' => 'position/hierarchy',
                        'as' => 'position.hierarchy.',
                    ],
                    function () {
                        Route::get('get', 'PositionsHierarchyController@get')
                            ->name('get');
                    }
                );
                Route::group(
                    [
                        'namespace' => 'Department',
                        'prefix' => 'department',
                        'as' => 'department.',
                    ],
                    function () {
                        Route::get('get-all', 'DepartmentController@getAll')
                            ->name('get_all');
                        Route::post('create', 'DepartmentController@create')
                            ->name('create');
                        Route::post('update', 'DepartmentController@update')
                            ->name('update');
                        Route::post('delete', 'DepartmentController@delete')
                            ->name('delete');
                        Route::post('restore', 'DepartmentController@restore')
                            ->name('restore');
                    }
                );
                Route::group(
                    [
                        'namespace' => 'Position',
                        'prefix' => 'position',
                        'as' => 'position.',
                    ],
                    function () {
                        Route::get('get-all', 'PositionController@getAll')
                            ->name('get_all');
                        Route::post('create', 'PositionController@create')
                            ->name('create');
                        Route::post('update', 'PositionController@update')
                            ->name('update');
                        Route::post('delete', 'PositionController@delete')
                            ->name('delete');
                        Route::post('restore', 'PositionController@restore')
                            ->name('restore');
                    }
                );
                Route::group(
                    [
                        'namespace' => 'Employee',
                        'prefix' => 'employee',
                        'as' => 'employee.',
                    ],
                    function () {
                        Route::get('get-all', 'EmployeeController@getAll')
                            ->name('get_all');
                        Route::post('create', 'EmployeeController@create')
                            ->name('create');
                        Route::post('update', 'EmployeeController@update')
                            ->name('update');
                        Route::post('delete', 'EmployeeController@delete')
                            ->name('delete');
                        Route::post('restore', 'EmployeeController@restore')
                            ->name('restore');
                    }
                );

                Route::group(
                    [
                        'namespace' => 'LegalHoliday',
                        'prefix' => 'legal-holiday',
                        'as' => 'legal_holiday.',
                    ],
                    function () {
                        Route::post('create', 'LegalHolidayController@create')->name('create');
                        Route::post('update', 'LegalHolidayController@update')->name('update');
                        Route::post('delete', 'LegalHolidayController@delete')->name('delete');
                    }
                );

                Route::group(
                    [
                        'namespace' => 'PersonalTimeOff',
                    ],
                    function () {
                        Route::group(
                            [
                                'namespace' => 'PaidLeave',
                                'prefix' => 'paid-leave',
                                'as' => 'paid_leave.',
                            ],
                            function () {
                                Route::post('approve', 'PaidLeaveController@approve')
                                    ->name('approve');
                                Route::post('reject', 'PaidLeaveController@reject')
                                    ->name('reject');
                            }
                        );

                        Route::group(
                            [
                                'namespace' => 'SickLeave',
                                'prefix' => 'sick-leave',
                                'as' => 'sick_leave.',
                            ],
                            function () {
                                Route::post('approve', 'SickLeaveController@approve')
                                    ->name('approve');
                                Route::post('reject', 'SickLeaveController@reject')
                                    ->name('reject');
                            }
                        );
                    }
                );

                Route::group(
                    [
                        'namespace' => 'WorkFromHome',
                        'prefix' => 'work-from-home',
                        'as' => 'work_from_home.',
                    ],
                    function () {
                        Route::post('approve', 'WorkFromHomeController@approve')
                            ->name('approve');
                        Route::post('reject', 'WorkFromHomeController@reject')
                            ->name('reject');
                    }
                );
            }
        );
    }
);