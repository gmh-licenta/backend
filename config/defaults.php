<?php

return [
    'department' => [
        'id' => 1,
    ],
    'position' => [
        'id' => 1,
    ],
    'users' => [
        'super_admin_id' => 1,
        'new_user_role' => 'Employee',
        'new_user_password' => 'asd123',
    ],
    'roles' => [
        'super_admin_id' => 1,
        'employee_id' => 4,
    ],
    'passport' => [
        'clients' => [
            'store' => [
                'user_id' => null,
                'confidential' => true,
//                'personal_access_client' => false,
//                'password_client' => false,
            ]
        ]
    ]
];
