<?php

return [
    [
        'option' => 'initialized',
        'value' => 'false',
//        'value' => 'true',
        'text' => '',
    ],
    [
        'option' => 'token_lifetime',
        'value' => '30 minutes',
//        'value' => '12 months',
        'text' => 'How long must an user be inactive for until automatic logout will be performed?'
    ],
    [
        'option' => 'country',
        'value' => 'RO',
        'text' => 'Please select a country'
    ],
//    [
//        'option' => 'legal_holidays',
//        'value' => '',
//        'text' => 'This is a default list of both public and observance legal holidays from %s. Feel free to customize it however you want!',
//    ],
];