<?php

namespace App\Providers;

use Api\Config\Config as ApiConfig;
use Carbon\Carbon;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Schema;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        $now = Carbon::now();

        if (Schema::hasTable('config'))
        {
            $tokenLifetimeString = ApiConfig::where('option', 'token_lifetime')->first()->value;
        }
        else
        {
            $tokenLifetimeString = "30 minutes";
        }

        //token lifetime
        [$tokenLifetimeValue, $tokenLifetimeUnits] = explode(' ', $tokenLifetimeString);
        $tokenLifetime = $now
            ->copy()
            ->add($tokenLifetimeValue, $tokenLifetimeUnits);
        Passport::tokensExpireIn($tokenLifetime);
        Passport::refreshTokensExpireIn($tokenLifetime);

        // Implicitly grant "Super Admin" role all permission checks using can()
        Gate::before(function ($user, $ability) {
            if ($user->hasRole('SuperAdmin')) {
                return true;
            }
        });
    }
}
