<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        header('Access-Control-Allow-Origin:  *');
//        header('Access-Control-Allow-Origin:  http://192.168.0.108:8080');
        header('Access-Control-Allow-Headers:  *');
//        header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Authorization, Origin');
        header('Access-Control-Allow-Methods:  *');
//        header('Access-Control-Allow-Methods:  POST, PUT, PATCH, DELETE');
        header('Access-Control-Expose-Headers: X-Status-Reason');

        return $next($request);
    }
}
