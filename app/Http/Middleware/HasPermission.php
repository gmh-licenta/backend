<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;

class HasPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $loggedInUser = Auth::user();

        $routeName = $request->route()->getName();
        $routeName = substr($routeName, 4);

        $permission = Permission::where('name', $routeName)->first();

        foreach ($loggedInUser->roles as $role)
        {
            if ($role->name === 'SuperAdmin')
            {
                return $next($request);
            }

            if ($role->hasPermissionTo($permission))
            {
                return $next($request);
            }
        }

        return response()->json([
            [],
            Response::HTTP_UNAUTHORIZED,
            [
                'X-Status-Reason' => 'Access Denied',
            ]
        ]);
    }
}
