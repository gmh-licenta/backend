<?php


namespace Api\WorkFromHome;


use Api\PersonalTimeOff\PaidLeave\PaidLeave;
use Api\PersonalTimeOff\PaidLeave\PaidLeaveTransformer;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class WorkFromHomeService
{
    /**
     * @var WorkFromHome
     */
    protected $workFromHomeModel;

    /**
     * @var WorkFromHomeTransformer
     */
    protected $workFromHomeTransformer;

    /**
     * @var string
     */
    protected $defaultSuperAdminId;

    public function __construct(WorkFromHome $workFromHomeModel,
        WorkFromHomeTransformer $workFromHomeTransformer)
    {
        $this->workFromHomeModel = $workFromHomeModel;
        $this->workFromHomeTransformer = $workFromHomeTransformer;

        $defaults = Config::get('defaults');
        $this->defaultSuperAdminId = $defaults['users']['super_admin_id'];
    }

    public function create(array $data)
    {
        $loggedInUser = Auth::user();
        $data['user_id'] = $loggedInUser->id;

        $data = $this->workFromHomeTransformer->workFromHome($data);

        $workFromHomeRequest = $this->workFromHomeModel->create($data);

        return $workFromHomeRequest;
    }

    public function get(array $query)
    {
        $loggedInUser = Auth::user();

        $adminRole = Role::where('name', 'Admin')->first();

        $workFromHomeRequests = $this->workFromHomeModel;

        //single user
        if (array_key_exists('user_id', $query))
        {
            $workFromHomeRequests = $workFromHomeRequests->where('user_id', $query['user_id']);
        }
        //if admin, get all without the ones for other admins including self
        else if ($loggedInUser->roles->first()->name === $adminRole->name)
        {
            $admins = DB::table('model_has_roles')
                ->where('role_id', $adminRole->id)
                ->pluck('model_id');

            $workFromHomeRequests = $workFromHomeRequests->whereNotIn('user_id', $admins);
        }
        //if not admin && id != superAdminId => employee, get only self records
        else if ($loggedInUser->id !== $this->defaultSuperAdminId)
        {
            $workFromHomeRequests = $workFromHomeRequests->where('user_id', $loggedInUser->id);
        }

        if (array_key_exists('date', $query))
        {
            $workFromHomeRequests = $workFromHomeRequests->where('start_date', '>=', $query['date'])
                ->where('end_date', '<=', $query['date']);

            unset($query['date']);
        }

        foreach ($query as $column => $value)
        {
            $workFromHomeRequests = $workFromHomeRequests->where($column, 'like', '%' . $value . '%');
        }

        $workFromHomeRequests = $workFromHomeRequests->get();

        $workFromHomeRequests = $workFromHomeRequests
            ->map(function ($workFromHome, $key) {
                $workFromHome->user = $workFromHome->user;
                $workFromHome->user_changed_status = $workFromHome->userChangedStatus;

                $workFromHome->start_date_display = $workFromHome->start_date->copy()->format('Y-m-d');
                $workFromHome->end_date_display = $workFromHome->end_date->copy()->format('Y-m-d');

                return $workFromHome;
            })
            ->groupBy('status')
            ->map(function ($workFromHomeRequestsByStatus, $status) {
                $workFromHomeRequestsByStatus = $workFromHomeRequestsByStatus
                    ->sortByDesc('start_date')
                    ->values()
                ;
                return $workFromHomeRequestsByStatus;
            });

        if (!$workFromHomeRequests->has(WorkFromHome::STATUS_UNPROCESSED))
        {
            $workFromHomeRequests->put(WorkFromHome::STATUS_UNPROCESSED, []);
        }
        if (!$workFromHomeRequests->has(WorkFromHome::STATUS_APPROVED))
        {
            $workFromHomeRequests->put(WorkFromHome::STATUS_APPROVED, []);
        }
        if (!$workFromHomeRequests->has(WorkFromHome::STATUS_REJECTED))
        {
            $workFromHomeRequests->put(WorkFromHome::STATUS_REJECTED, []);
        }

        return $workFromHomeRequests;
    }

    public function approve(array $data)
    {
        $loggedInUser = Auth::user();

        if (!array_key_exists('id', $data))
        {
            return [
                'error' => true,
                'message' => "Missing Parameter 'id'",
            ];
        }
        $workFromHomeRequest = $this->workFromHomeModel->find($data['id']);

        DB::beginTransaction();

        try
        {
            $workFromHomeRequest->update([
                'status' => WorkFromHome::STATUS_APPROVED,
                'status_changed_by_user_id' => $loggedInUser->id,
                'status_date' => Carbon::now()->toDateTimeString(),
            ]);

            DB::commit();

            return $workFromHomeRequest;
        }
        catch (\Exception $e)
        {
            DB::rollBack();

            return [
                'error' => true,
                'message' => 'Failed To Approve Work From Home Request',
            ];
        }
    }

    public function reject(array $data)
    {
        $loggedInUser = Auth::user();

        if (!array_key_exists('id', $data))
        {
            return [
                'error' => true,
                'message' => "Missing Parameter 'id'",
            ];
        }
        $workFromHomeRequest = $this->workFromHomeModel->find($data['id']);

        DB::beginTransaction();

        try
        {
            $workFromHomeRequest->update([
                'status' => WorkFromHome::STATUS_REJECTED,
                'status_changed_by_user_id' => $loggedInUser->id,
                'status_date' => Carbon::now()->toDateTimeString(),
            ]);

            DB::commit();

            return $workFromHomeRequest;
        }
        catch (\Exception $e)
        {
            DB::rollBack();

            return [
                'error' => true,
                'message' => 'Failed To Reject Work From Home Request',
            ];
        }
    }
}