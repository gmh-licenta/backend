<?php


namespace Api\WorkFromHome;


use Api\Config\Config as ApiConfig;
use Api\Helpers\Helpers;
use Api\LegalHoliday\LegalHoliday;
use Carbon\Carbon;

class WorkFromHomeTransformer
{
    public function workFromHome(array $data): array
    {
        $startDate = Carbon::parse($data['startDate'])->startOfDay();
        $endDate = Carbon::parse($data['endDate'])->endOfDay();

        $days = Helpers::date()->getNumberOfWorkingDaysInInterval($startDate, $endDate);

        $data['start_date'] = $startDate;
        $data['end_date'] = $endDate;
        $data['days'] = $days;
        $data['status'] = WorkFromHome::STATUS_UNPROCESSED;
        $data['status_date'] = Carbon::now();

        unset($data['startDate']);
        unset($data['endDate']);

        return $data;
    }
}