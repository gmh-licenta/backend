<?php

namespace Api\WorkFromHome;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class WorkFromHomeController extends Controller
{
    /**
     * @var WorkFromHomeService
     */
    protected $workFromHomeService;

    public function __construct(WorkFromHomeService $workFromHomeService)
    {
        $this->workFromHomeService = $workFromHomeService;
    }

    public function create(Request $request)
    {
        $data = $request->all();

        $workFromHomeRequest = $this->workFromHomeService->create($data);

        if ($workFromHomeRequest instanceof WorkFromHome)
        {
            return response()->json(
                [],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Work From Home Request Registered Successfully',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Work From Home Request Failed To Be Registered',
                ]
            );
        }

    }

    public function get(Request $request)
    {
        $query = $request->query();
        $workFromHomeRequests = $this->workFromHomeService->get($query);

        return response()->json(
            [
                'workFromHomeRequests' => $workFromHomeRequests
            ],
            Response::HTTP_OK,
            [
                'X-Status-Reason' => 'Work From Home Requests For User',
            ]
        );
    }

    public function approve(Request $request)
    {
        $data = $request->all();
        $response = $this->workFromHomeService->approve($data);

        if ($response instanceof WorkFromHome)
        {
            return response()->json(
                [
                    'workFromHomeRequest' => $response,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Work From Home Request Approved Successfully',
                ]
            );
        }
        else if (array_key_exists($response['error']) && $response['error'] === true)
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => $response['message'],
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Approve Work From Home Request',
                ]
            );
        }
    }
    public function reject(Request $request)
    {

        $data = $request->all();
        $response = $this->workFromHomeService->reject($data);

        if ($response instanceof WorkFromHome)
        {
            return response()->json(
                [
                    'workFromHomeRequest' => $response,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Work From Home Request Rejected Successfully',
                ]
            );
        }
        else if (array_key_exists($response['error']) && $response['error'] === true)
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => $response['message'],
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Reject Work From Home Request',
                ]
            );
        }
    }
}