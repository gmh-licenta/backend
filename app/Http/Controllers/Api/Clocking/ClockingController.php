<?php


namespace Api\Clocking;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ClockingController extends Controller
{
    /**
     * @var ClockingService
     */
    protected $clockingService;

    public function __construct(ClockingService $clockingService)
    {
        $this->clockingService = $clockingService;
    }

    public function getForCurrentUser()
    {
        $clockings = $this->clockingService->getForCurrentUser();
        $filters = $this->clockingService->getForCurrentUserFilters($clockings);

        if ($clockings->count() === 0)
        {
            return response()->json(
                [
                    'clockings' => [],
                    'filters' => [],
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Clockings Not Found',
                ]
            );
        }

        return response()->json(
            [
                'clockings' => $clockings,
                'filters' => $filters,
            ],
            Response::HTTP_OK,
            [
                'X-Status-Reason' => 'Clockings Found',
            ]
        );
    }

    public function getCurrentWorkingDayClocking()
    {
        $clocking = $this->clockingService->getCurrentWorkingDayClocking();

        if ($clocking === nulL)
        {
            return response()->json(
                [
                    'clocking' => null,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Clocking Not Found',
                ]
            );
        }

        return response()->json(
            [
                'clocking' => $clocking
            ],
            Response::HTTP_OK,
            [
                'X-Status-Reason' => 'Clocking Found',
            ]
        );
    }

    public function clockIn(Request $request)
    {
        $data = $request->all();

        $clockIn = $this->clockingService->clockIn($data);

        if ($clockIn instanceof Clocking)
        {
            return response()->json(
                [],
//                Response::HTTP_CREATED,
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Clock In Registered Successfully',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Clock In Failed To Be Registered',
                ]
            );
        }
    }

    public function clockOut(Request $request)
    {
        $data = $request->all();
        $status = $this->clockingService->clockOut($data);

        if ($status)
        {
            return response()->json(
                [],
//                Response::HTTP_RESET_CONTENT,
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Clock Out Registered Successfully',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Clock Out Failed To Be Registered',
                ]
            );
        }
    }
}