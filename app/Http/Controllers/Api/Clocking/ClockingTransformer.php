<?php


namespace Api\Clocking;


use Carbon\Carbon;

class ClockingTransformer
{
    public function clockIn(array $data): array
    {
        if (isset($data['date']))
        {
            $clockIn = Carbon::parse($data['date']);
        }
        else
        {
            $clockIn = Carbon::now();
        }

        $clockIn = $clockIn->setTimeFromTimeString($data['time']);
        $data['clock_in'] = $clockIn;

        unset($data['date']);
        unset($data['time']);

        return $data;
    }

    public function clockOut(array $data): array
    {
        $response = [];

        if (isset($data['date']))
        {
            $clockOut = Carbon::parse($data['date']);
        }
        else
        {
            $clockOut = Carbon::now();
        }

        $clockOut = $clockOut->setTimeFromTimeString($data['time']);
        $response['clock_out'] = $clockOut;

        return $response;
    }
}