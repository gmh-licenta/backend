<?php


namespace Api\Clocking;


use Carbon\Carbon;
use Carbon\CarbonInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class ClockingService
{
    /**
     * @var Clocking
     */
    protected $clockingModel;

    /**
     * @var ClockingTransformer
     */
    protected $clockingTransformer;

    public function __construct(Clocking $clockingModel, ClockingTransformer $clockingTransformer)
    {
        $this->clockingModel = $clockingModel;
        $this->clockingTransformer = $clockingTransformer;
    }

    public function clockIn(array $data)
    {
        $loggedInUser = Auth::user();
        $data['user_id'] = $loggedInUser->id;

        $data = $this->clockingTransformer->clockIn($data);

        $clockInEntry = $this->clockingModel->create($data);

        return $clockInEntry;
    }

    public function clockOut(array $data)
    {
        $data = $this->clockingTransformer->clockOut($data);

        $clockInEntry = $this->getCurrentWorkingDayClocking();

        if ($clockInEntry === null)
        {
            $updateStatus = false;
        }
        else
        {
            $updateStatus = $clockInEntry->update($data);
        }

        return $updateStatus;
    }

    public function getCurrentWorkingDayClocking()
    {
        $loggedInUser = Auth::user();

        $userClocking = $this->clockingModel
                ->where('user_id', $loggedInUser->id)
                ->orderBy('id', 'desc')
                ->first();

        if ($this->isCurrentWorkingDayClocking($userClocking))
        {
            return $userClocking;
        }

        return null;
    }

    /**
     * Check if the clocking instance is still part of the current working day or not
     * A working day must also cover night shifts (work shift starting today and ending tomorrow)
     *
     * 1. If the user clocks in today, he should be able to clock out tomorrow (considering the
     * 24h mark hasn't been exceeded and clock_out === null)
     * 2. If the user clocked in today and clocked out tomorrow, he should be able to clock in
     * again tomorrow.
     *
     * @param Clocking|null $clocking
     *
     * @return bool
     */
    public function isCurrentWorkingDayClocking(?Clocking $clocking)
    {
        if ($clocking === null)
        {
            return false;
        }

        $now = Carbon::now()
//            ->addDay()
//            ->subHours(1)
            ->setSeconds(0)
            ->setMicroseconds(0);

        /**
         * 1
         * If the 24h mark has been exceeded while comparing now to the clock_in date, no matter
         * if the user clocked out or not, the clocking entry is considered to be no longer
         * associated with the 'current working day'
         */
        $hourDiff = $now->diffInHours($clocking->clock_in);

        if ($hourDiff >= 24)
        {
            return false;
        }

        /**
         * 2
         * If the user did clock out on this clocking entry and the day changed comparing now to
         * the clock_in date (we're in the next day following the clock in day), even if the user
         * did clock out today, his next shift would also start today, so we can assume the
         * current shift (yesterday-today) is done and we can consider the clocking entry is no
         * longer associated with the 'current working day'
         */
        $today = $now->day;
        $clockInDay = $clocking->clock_in->day;

        if ($today > $clockInDay && $clocking->clock_out !== null)
        {
            return false;
        }

        return true;
    }

    public function getForCurrentUser()
    {
        $loggedInUser = Auth::user();

        $clockings = $this->clockingModel
            ->where('user_id', $loggedInUser->id)
            ->orderBy('clock_in', 'desc')
            ->get();

        $clockings = $clockings->map(function ($clocking, $key) {
            if ($clocking->clock_out === null)
            {
                $clocking->time = "Not Set";

                $clocking->clock_out_date = "Not Set";
                $clocking->clock_out_time = "Not Set";
            }
            else
            {
                $clocking->time = $clocking->clock_out->diffForHumans($clocking->clock_in,
                    CarbonInterface::DIFF_ABSOLUTE, false, 2);


                $clocking->clock_out_date = $clocking->clock_out->copy()->format('Y-m-d');
                $clocking->clock_out_time = $clocking->clock_out->copy()->format('H:i');
            }

            $clocking->day = $clocking->clock_in->englishDayOfWeek;
            $clocking->month = $clocking->clock_in->englishMonth;
            $clocking->year = $clocking->clock_in->year;

            $clocking->clock_in_date = $clocking->clock_in->copy()->format('Y-m-d');
            $clocking->clock_in_time = $clocking->clock_in->copy()->format('H:i');

            return $clocking;
        });

        return $clockings;
    }

    public function getForCurrentUserFilters(Collection $clockings)
    {
        $filters = [];

        $byDay = $this->getForCurrentUserDayFilter($clockings);
        $byMonth = $this->getForCurrentUserMonthFilter($clockings);
        $byYear = $this->getForCurrentUserYearFilter($clockings);

        $filters['day'] = $byDay;
        $filters['month'] = $byMonth;
        $filters['year'] = $byYear;

        return $filters;
    }

    public function getForCurrentUserDayFilter(Collection $clockings)
    {
        $filter = $clockings
            ->groupBy(function ($clocking, $key) {
                return $clocking->day;
            })
            ->keys();

        return $filter;
    }

    public function getForCurrentUserMonthFilter(Collection $clockings)
    {
        $filter = $clockings
            ->groupBy(function ($clocking, $key) {
                return $clocking->month;
            })
            ->keys();

        return $filter;
    }

    public function getForCurrentUserYearFilter(Collection $clockings)
    {
        $filter = $clockings
            ->groupBy(function ($clocking, $key) {
                return $clocking->year;
            })
            ->keys();

        return $filter;
    }
}