<?php

namespace Api\Department\Hierarchy;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DepartmentsHierarchyController extends Controller
{
    /**
     * @var DepartmentsHierarchyService
     */
    protected $departmentsHierarchyService;

    public function __construct(DepartmentsHierarchyService $departmentsHierarchyService)
    {
        $this->departmentsHierarchyService = $departmentsHierarchyService;
    }

    public function get(Request $request)
    {
        $query = $request->query();

        $departments = $this->departmentsHierarchyService->get($query);

        return response()->json(
            [
                'departments' => $departments,
            ],
            Response::HTTP_OK,
            []
        );
    }

    public function update(Request $request)
    {
        $data = $request->all();

        $status = $this->departmentsHierarchyService->update($data);

        if ($status === true)
        {
            return response()->json(
                [],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Departments Hierarchy Updated Successfully',
                ]
            );
        }
        else if (array_key_exists('error', $status))
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => $status['message'],
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed to update departments hierarchy',
                ]
            );
        }
    }
}