<?php


namespace Api\Department\Hierarchy;


use Api\Department\Department;
use Illuminate\Database\Eloquent\Model;

class DepartmentsHierarchy extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'departments_hierarchy';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }
}