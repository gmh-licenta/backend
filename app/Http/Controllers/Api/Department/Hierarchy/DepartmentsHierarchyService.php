<?php


namespace Api\Department\Hierarchy;


use Api\Department\Department;
use Api\UserProfile\UserProfile;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DepartmentsHierarchyService
{
    /**
     * @var DepartmentsHierarchy
     */
    protected $departmentsHierarchyModel;

    /**
     * @var Department
     */
    protected $departmentModel;

    /**
     * @var UserProfile
     */
    protected $userProfileModel;

    public function __construct(DepartmentsHierarchy $departmentsHierarchyModel, Department
    $departmentModel, UserProfile $userProfileModel)
    {
        $this->departmentsHierarchyModel = $departmentsHierarchyModel;
        $this->departmentModel = $departmentModel;

        $this->userProfileModel = $userProfileModel;
    }

    public function get(array $query)
    {
        $departments = $this->departmentsHierarchyModel;

        foreach ($query as $column => $value)
        {
            $departments = $departments->where($column, 'like', '%' . $value . '%');
        }

        $departments = $departments->get();

        $departments = $departments
            ->sortBy('index')
            ->map(function ($department, $key) {
                $department->name = $department->department->name;

                return $department;
            })
            ->values();

        return $departments;
    }

    public function update(array $data)
    {
        if (!array_key_exists('departments', $data))
        {
            return [
                'error' => true,
                'message' => "Parameter 'departments' could not be found."
            ];
        }

        $departmentsHierarchy = $data['departments'];

        DB::beginTransaction();

        try
        {
            foreach ($departmentsHierarchy as $department)
            {
                $this->departmentsHierarchyModel->where('id', $department['id'])
                    ->update([
                        'index' => $department['index'],
                    ]);
            }

            DB::commit();

            return true;
        }
        catch (\Exception $e)
        {
            DB::rollBack();

            return [
                'error' => true,
               'message' => 'Failed to update departments hierarchy',
            ];
        }
    }

    public function refresh()
    {
        $users = $this->userProfileModel
            ->all();

        $departments = $this->departmentsHierarchyModel
            ->all()
            ->pluck('id')
            ->toArray();

        $indexes = [];
        $processedDepartments = [];
        foreach ($users as $user)
        {
            if (array_key_exists($user->department->id, $indexes))
            {
                continue;
            }
            else
            {
                $indexes[] = $user->department->id;
            }

            $index = count($indexes) + 1;

            $departmentExists = $this->departmentsHierarchyModel
                ->where('department_id', $user->department->id)
                ->first();

            if ($departmentExists !== null)
            {
                $processedDepartments[] = $departmentExists->id;
                continue;
            }

            $newDepartment = $this->departmentsHierarchyModel->create([
                'department_id' => $user->department->id,
                'index' => $index,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]);

            $processedDepartments[] = $newDepartment->id;

            $indexes[] = $user->department->id;
        }

        $departmentsToDelete = array_diff($departments, $processedDepartments);
        $this->departmentsHierarchyModel->whereIn('id', $departmentsToDelete)->delete();
    }
}