<?php

namespace Api\Department;

use Api\Department\Hierarchy\DepartmentsHierarchyService;
use Api\Position\Hierarchy\PositionsHierarchyService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DepartmentController extends Controller
{
    /**
     * @var DepartmentService
     */
    protected $departmentService;

    /**
     * @var DepartmentsHierarchyService
     */
    protected $departmentsHierarchyService;

    /**
     * @var PositionsHierarchyService
     */
    protected $positionsHierarchyService;

    public function __construct(DepartmentService $departmentService, DepartmentsHierarchyService
    $departmentsHierarchyService, PositionsHierarchyService $positionsHierarchyService)
    {
        $this->departmentService = $departmentService;

        $this->departmentsHierarchyService = $departmentsHierarchyService;
        $this->positionsHierarchyService = $positionsHierarchyService;
    }

    public function getAll()
    {
        $departments = $this->departmentService->getAll();

        return response()->json(
            [
                'departments' => [
                    'active' => $departments->get('active'),
                    'deleted' => $departments->get('deleted'),
                ],
            ],
            Response::HTTP_OK
        );
    }

    public function create(Request $request)
    {
        $data = $request->all();

        $department = $this->departmentService->create($data);

        if ($department instanceof Department)
        {
            return response()->json(
                [
                    'department' => $department,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Department Added Successfully',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Add New Department',
                ]
            );
        }
    }

    public function update(Request $request)
    {
        $data = $request->all();

        $response = $this->departmentService->update($data);
        $status = $response['status'];
        $department = $response['department'];

        if ($status === true)
        {
            return response()->json(
                [
                    'department' => $department,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Department Updated Successfully',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Update Department',
                ]
            );
        }
    }

    public function delete(Request $request)
    {
        $data = $request->all();

        $status = $this->departmentService->delete($data);

        $this->departmentsHierarchyService->refresh();
        $this->positionsHierarchyService->refresh();

        if ($status === true)
        {
            return response()->json(
                [],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Department Deleted Successfully',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Delete Department',
                ]
            );
        }
    }

    public function restore(Request $request)
    {
        $data = $request->all();

        $response = $this->departmentService->restore($data);

        $this->departmentsHierarchyService->refresh();
        $this->positionsHierarchyService->refresh();

        $status = $response['status'];
        $department = $response['department'];

        if ($status === true)
        {
            return response()->json(
                [
                    'department' => $department,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Department Restored Successfully',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Restore Department',
                ]
            );
        }
    }
}