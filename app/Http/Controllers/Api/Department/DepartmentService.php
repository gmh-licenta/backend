<?php

namespace Api\Department;


use Api\Department\Hierarchy\DepartmentsHierarchy;
use Api\Position\Hierarchy\PositionsHierarchy;
use Api\UserProfile\UserProfile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class DepartmentService
{
    /**
     * @var Department
     */
    protected $departmentModel;

    /**
     * @var DepartmentsHierarchy
     */
    protected $departmentsHierarchyModel;

    /**
     * @var PositionsHierarchy
     */
    protected $positionsHierarchyModel;

    /**
     * @var UserProfile
     */
    protected $userProfileModel;

    /**
     * @var int
     */
    protected $defaultDepartmentId;

    public function __construct(Department $departmentModel, DepartmentsHierarchy
    $departmentsHierarchyModel, PositionsHierarchy $positionsHierarchyModel, UserProfile
    $userProfileModel)
    {
        $this->departmentModel = $departmentModel;
        $this->departmentsHierarchyModel = $departmentsHierarchyModel;
        $this->positionsHierarchyModel = $positionsHierarchyModel;
        $this->userProfileModel = $userProfileModel;

        $this->defaultDepartmentId = Config::get('defaults.department')['id'];
    }

    public function getAll(bool $withDefault = false)
    {
        $departments = $this->departmentModel
            ->withTrashed();

        if (!$withDefault)
        {
            $departments = $departments->where('id', '!=', $this->defaultDepartmentId);
        }

        $departments = $departments->get();

        $departments = $departments->groupBy(function ($department, $key) {
            if ($department->deleted_at !== null)
            {
                return 'deleted';
            }

            return 'active';
        });

        if (!$departments->has('active'))
        {
            $departments->put('active', collect());
        }
        if (!$departments->has('deleted'))
        {
            $departments->put('deleted', collect());
        }

        return $departments;
    }

    public function create(array $data)
    {
        $department = $this->departmentModel->create($data);

        return $department;
    }

    public function update(array $data)
    {
        $department = $this->departmentModel->find($data['id']);

        $status = $department
            ->update([
                'name' => $data['name'],
            ]);

        $response = [
            'status' => $status,
            'department' => $department,
        ];

        return $response;
    }

    public function delete(array $data)
    {
        if ($data['id'] == $this->defaultDepartmentId)
        {
            return false;
        }

        $department = $this->departmentModel->find($data['id']);

        DB::beginTransaction();

        try
        {
            $status = $this->userProfileModel
                ->where('department_id', $data['id'])
                ->update([
                    'department_id' => $this->defaultDepartmentId,
                ]);

            if ($status === false)
            {
                DB::rollBack();

                return [
                    'error' => true,
                    'message' => 'Failed To Reset Users Departments',
                ];
            }

//            $status = $this->departmentsHierarchyModel
//                ->where('department_id', $data['id'])
//                ->delete();
//
//            if ($status === false)
//            {
//                DB::rollBack();
//
//                return [
//                    'error' => true,
//                    'message' => 'Failed To Delete Department From Hierarchy',
//                ];
//            }
//
//            $status = $this->positionsHierarchyModel
//                ->where('department_id', $data['id'])
//                ->delete();
//
//            if ($status === false)
//            {
//                DB::rollBack();
//
//                return [
//                    'error' => true,
//                    'message' => 'Failed To Delete Department From Positions Hierarchy',
//                ];
//            }

            $status = $department->delete();

            if ($status === false)
            {
                DB::rollBack();

                return [
                    'error' => true,
                    'message' => 'Failed To Delete Department',
                ];
            }

            DB::commit();

            return true;
        }
        catch (\Exception $e)
        {
            DB::rollBack();
//            $this->userProfileModel
//                ->whereIn('id', $usersProfilesIds)
//                ->update([
//                    'department_id' => $data['id'],
//                ]);
//
//            $this->departmentModel->create([
//                'id' => $cloneDepartment->id,
//                'name' => $cloneDepartment->name,
//                'created_at' => $cloneDepartment->created_at,
//                'updated_at' => $cloneDepartment->updated_at,
//            ]);

            return false;
        }
    }

    public function restore(array $data)
    {
        $department = $this->departmentModel
            ->withTrashed()
            ->find($data['id']);

        $status = $department->restore();

//        $this->departmentsHierarchyModel->withTrashed()
//            ->where('department_id', $data['id'])
//            ->restore();

        return [
            'status' => $status,
            'department' => $department,
        ];
    }
}