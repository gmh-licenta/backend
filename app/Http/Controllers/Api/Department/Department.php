<?php

namespace Api\Department;

use Api\Department\Hierarchy\DepartmentsHierarchy;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'departments';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function userProfile()
    {
        return $this->belongsTo('Api\UserProfile');
    }

    public function hierarchy()
    {
        return $this->hasOne(DepartmentsHierarchy::class, 'id', 'department_id');
    }
}