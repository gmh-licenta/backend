<?php

namespace Api\Position;

use Api\Department\Hierarchy\DepartmentsHierarchyService;
use Api\Position\Hierarchy\PositionsHierarchyService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PositionController extends Controller
{
    /**
     * @var PositionService
     */
    protected $positionService;

    /**
     * @var DepartmentsHierarchyService
     */
    protected $departmentsHierarchyService;

    /**
     * @var PositionsHierarchyService
     */
    protected $positionsHierarchyService;

    public function __construct(PositionService $positionService, DepartmentsHierarchyService
    $departmentsHierarchyService, PositionsHierarchyService $positionsHierarchyService)
    {
        $this->positionService = $positionService;

        $this->departmentsHierarchyService = $departmentsHierarchyService;
        $this->positionsHierarchyService = $positionsHierarchyService;
    }

    public function getAll()
    {
        $positions = $this->positionService->getAll();

        return response()->json(
            [
                'positions' => [
                    'active' => $positions->get('active'),
                    'deleted' => $positions->get('deleted'),
                ],
            ],
            Response::HTTP_OK
        );
    }

    public function create(Request $request)
    {
        $data = $request->all();

        $position = $this->positionService->create($data);

        if ($position instanceof Position)
        {
            return response()->json(
                [
                    'position' => $position,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Position Added Successfully',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Add New Position',
                ]
            );
        }
    }

    public function update(Request $request)
    {
        $data = $request->all();

        $response = $this->positionService->update($data);
        $status = $response['status'];
        $position = $response['position'];

        if ($status === true)
        {
            return response()->json(
                [
                    'position' => $position,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Position Updated Successfully',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Update Position',
                ]
            );
        }
    }

    public function delete(Request $request)
    {
        $data = $request->all();

        $status = $this->positionService->delete($data);

        $this->departmentsHierarchyService->refresh();
        $this->positionsHierarchyService->refresh();

        if ($status === true)
        {
            return response()->json(
                [],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Position Deleted Successfully',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Delete Position',
                ]
            );
        }
    }

    public function restore(Request $request)
    {
        $data = $request->all();

        $response = $this->positionService->restore($data);

        $this->departmentsHierarchyService->refresh();
        $this->positionsHierarchyService->refresh();

        $status = $response['status'];
        $position = $response['position'];

        if ($status === true)
        {
            return response()->json(
                [
                    'position' => $position,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Position Restored Successfully',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Restore Position',
                ]
            );
        }
    }
}