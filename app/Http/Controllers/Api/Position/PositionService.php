<?php

namespace Api\Position;

use Api\UserProfile\UserProfile;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class PositionService
{
    /**
     * @var Position
     */
    protected $positionModel;
    
    /**
     * @var UserProfile
     */
    protected $userProfileModel;

    /**
     * @var int
     */
    protected $defaultPositionId;

    public function __construct(Position $positionModel, UserProfile $userProfileModel)
    {
        $this->positionModel = $positionModel;
        $this->userProfileModel = $userProfileModel;

        $this->defaultPositionId = Config::get('defaults.position')['id'];
    }

    public function getAll(bool $withDefault = false)
    {
        $positions = $this->positionModel
            ->withTrashed();

        if (!$withDefault)
        {
            $positions = $positions->where('id', '!=', $this->defaultPositionId);
        }

        $positions = $positions->get();

        $positions = $positions->groupBy(function ($position, $key) {
            if ($position->deleted_at !== null)
            {
                return 'deleted';
            }

            return 'active';
        });

        if (!$positions->has('active'))
        {
            $positions->put('active', collect());
        }
        if (!$positions->has('deleted'))
        {
            $positions->put('deleted', collect());
        }

        return $positions;
    }

    public function create(array $data)
    {
        $position = $this->positionModel->create($data);

        return $position;
    }

    public function update(array $data)
    {
        $position = $this->positionModel->find($data['id']);

        $status = $position
            ->update([
                'name' => $data['name'],
            ]);

        $response = [
            'status' => $status,
            'position' => $position,
        ];

        return $response;
    }

    public function delete(array $data)
    {
        if ($data['id'] == $this->defaultPositionId)
        {
            return false;
        }

        $position = $this->positionModel->find($data['id']);
//        $clonePosition = clone $position;
//
//        $usersProfilesIds = clone $this->userProfileModel
//            ->where('position_id', $data['id'])
//            ->pluck('id');

        DB::beginTransaction();

        try
        {
            $status = $this->userProfileModel
                ->where('position_id', $data['id'])
                ->update([
                    'position_id' => $this->defaultPositionId,
                ]);

            if ($status === false)
            {
                DB::rollBack();

                return [
                    'error' => true,
                    'message' => 'Failed To Reset Users Positions',
                ];
            }

            $status = $position->delete();

            if ($status === false)
            {
                DB::rollBack();

                return [
                    'error' => true,
                    'message' => 'Failed To Delete Position',
                ];
            }

            DB::commit();

            return true;
        }
        catch (\Exception $e)
        {
            DB::rollBack();

//            $this->userProfileModel
//                ->whereIn('id', $usersProfilesIds)
//                ->update([
//                    'position_id' => $data['id'],
//                ]);
//
//            $this->positionModel->create([
//                'id' => $clonePosition->id,
//                'name' => $clonePosition->name,
//                'created_at' => $clonePosition->created_at,
//                'updated_at' => $clonePosition->updated_at,
//            ]);

            return false;
        }
    }

    public function restore(array $data)
    {
        $position = $this->positionModel
            ->withTrashed()
            ->find($data['id']);

        $status = $position->restore();

        return [
            'status' => $status,
            'position' => $position,
        ];
    }
}