<?php


namespace Api\Position\Hierarchy;


use Api\Department\Hierarchy\DepartmentsHierarchy;
use Api\UserProfile\UserProfile;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PositionsHierarchyService
{
    /**
     * @var PositionsHierarchy
     */
    protected $positionsHierarchyModel;

    /**
     * @var DepartmentsHierarchy
     */
    protected $departmentsHierarchyModel;

    /**
     * @var UserProfile
     */
    protected $userProfileModel;

    public function __construct(PositionsHierarchy $positionsHierarchyModel, DepartmentsHierarchy
    $departmentsHierarchyModel, UserProfile $userProfileModel)
    {
        $this->positionsHierarchyModel = $positionsHierarchyModel;
        $this->departmentsHierarchyModel = $departmentsHierarchyModel;

        $this->userProfileModel = $userProfileModel;
    }


    public function get(array $query)
    {
        $positions = $this->positionsHierarchyModel;

        foreach ($query as $column => $value)
        {
            $positions = $positions->where($column, 'like', '%' . $value . '%');
        }

        $positions = $positions->get();

        $departmentsHierarchy = $this->departmentsHierarchyModel
            ->all()
            ->sortBy('index')
            ->pluck('department_id')
            ->toArray();

        $positions = $positions
            ->groupBy('department_id')
            ->map(function ($departmentPositions, $departmentId) {
                $departmentPositions = $departmentPositions
                    ->map(function ($position, $key) {
                        $position->name = $position->position->name;
                        $position->department_name = $position->department->name;

                        return $position;
                    })
                    ->unique('position_id')
                    ->sortBy('index')
                    ->values();

                return $departmentPositions;
            })
            ->sortBy(function ($departmentPositions, $departmentId) use ($departmentsHierarchy) {
//                return array_search($key1, $order) > array_search($key2, $order)
                return array_search($departmentId, $departmentsHierarchy);
            })
            ->values();

        return $positions;
    }

    public function update(array $data)
    {
        if (!array_key_exists('positions', $data))
        {
            return [
                'error' => true,
                'message' => "Parameter 'positions' could not be found."
            ];
        }

        $positionsHierarchy = $data['positions'];

        DB::beginTransaction();

        try
        {
            foreach ($positionsHierarchy as $position)
            {
                $this->positionsHierarchyModel->where('id', $position['id'])
                    ->update([
                        'index' => $position['index'],
                    ]);
            }

            DB::commit();

            return true;
        }
        catch (\Exception $e)
        {
            DB::rollBack();

            return [
                'error' => true,
                'message' => 'Failed to update positions hierarchy',
            ];
        }
    }

    public function refresh()
    {
        $users = $this->userProfileModel
            ->all();

        $positions = $this->positionsHierarchyModel
            ->all()
            ->pluck('id')
            ->toArray();

        $indexes = [];
        $processedPositions = [];
        foreach ($users as $user)
        {
            if (!array_key_exists($user->department->id, $indexes))
            {
                $indexes[$user->department->id] = [];
            }

            $index = count($indexes[$user->department->id]) + 1;

            if (in_array($user->position->id, $indexes[$user->department->id]))
            {
                continue;
            }

            $positionExists = $this->positionsHierarchyModel
                ->where('department_id', $user->department->id)
                ->where('position_id', $user->position->id)
                ->first();

            if ($positionExists)
            {
                $processedPositions[] = $positionExists->id;
                continue;
            }

            $newPosition = $this->positionsHierarchyModel::create([
                'department_id' => $user->department->id,
                'position_id' => $user->position->id,
                'index' => $index,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]);

            $processedPositions[] = $newPosition->id;

            $indexes[$user->department->id][] = $user->position->id;
        }

        $positionsToDelete = array_diff($positions, $processedPositions);
        $this->positionsHierarchyModel->whereIn('id', $positionsToDelete)->delete();
    }
}