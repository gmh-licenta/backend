<?php

namespace Api\Position\Hierarchy;


use Api\Department\Department;
use Api\Position\Position;
use Illuminate\Database\Eloquent\Model;

class PositionsHierarchy extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'positions_hierarchy';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function position()
    {
        return $this->belongsTo(Position::class, 'position_id', 'id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }
}