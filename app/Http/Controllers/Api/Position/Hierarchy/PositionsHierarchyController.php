<?php


namespace Api\Position\Hierarchy;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PositionsHierarchyController extends Controller
{
    /**
     * @var PositionsHierarchyService
     */
    protected $positionsHierarchyService;

    public function __construct(PositionsHierarchyService $positionsHierarchyService)
    {
        $this->positionsHierarchyService = $positionsHierarchyService;
    }

    public function get(Request $request)
    {
        $query = $request->query();

        $positions = $this->positionsHierarchyService->get($query);

        return response()->json(
            [
                'positions' => $positions,
            ],
            Response::HTTP_OK,
            []
        );
    }

    public function update(Request $request)
    {
        $data = $request->all();

        $status = $this->positionsHierarchyService->update($data);

        if ($status === true)
        {
            return response()->json(
                [],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Positions Hierarchy Updated Successfully',
                ]
            );
        }
        else if (array_key_exists('error', $status))
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => $status['message'],
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed to update positions hierarchy',
                ]
            );
        }
    }
}