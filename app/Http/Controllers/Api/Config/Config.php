<?php

namespace Api\Config;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config as ConfigFacade;

class Config extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'config';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public static function isAppInitialized()
    {
        return (self::where('option', 'initialized')->first()->value === 'true');
    }

    public static function getCountryCode()
    {
        return self::where('option', 'country')->first()->value;
    }

    public static function getMappedConfig()
    {
        $supportedCountries = ConfigFacade::get('api_calendarific.supported_countries');

        $config = self::all()
            ->mapWithKeys(function ($entry, $key) {
                $mappedEntry = [];

                if ($entry->option === 'country')
                {
                    $entry->option = 'country_code';
                }

                $mappedEntry[$entry->option] = $entry->value;

                return $mappedEntry;
            });

        $config = $config->put('country', $supportedCountries[$config['country_code']]);

        return $config;
    }
}