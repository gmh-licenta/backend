<?php


namespace Api\Config;

use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Support\Facades\Config as ConfigFacade;
use Api\Config\Config as ApiConfig;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Passport;

class ConfigService
{
    /**
     * @var Config
     */
    protected $configModel;

    /**
     * @var ConfigTransformer
     */
    protected $configTransformer;

    /**
     * @var array
     */
    protected $defaultConfig;

    public function __construct(Config $configModel, ConfigTransformer $configTransformer)
    {
        $this->configModel = $configModel;
        $this->configTransformer = $configTransformer;

        $this->defaultConfig = ConfigFacade::get('api_config');
    }

    public function get(array $query)
    {
        $config = $this->configModel;

        foreach ($query as $column => $value)
        {
            $config = $config->where($column, 'like', '%' . $value . '%');
        }

        $config = $config
            ->where('option', '!=', 'initialized')
            ->get();

        $config = $config
            ->mapWithKeys(function ($entry, $key) {
                $mappedEntry = [];
                $mappedEntry[$entry->option] = $entry->value;

                return $mappedEntry;
            });

        $supportedCountries = $this->getSupportedCountries();

        $response = [
            'config' => $config,
            'supportedCountries' => $supportedCountries,
        ];

        return $response;
    }

    public function getDefault()
    {
        $config = ConfigFacade::get('api_config');
        foreach ($config as $key => $entry)
        {
            if ($entry['option'] === 'initialized')
            {
                unset($config[$key]);
                break;
            }
        }

        $config = array_values($config);

        $supportedCountries = $this->getSupportedCountries();

        return [
            'config' => $config,
            'supportedCountries' => $supportedCountries,
        ];
    }

    public function isInitialized()
    {
        return ApiConfig::isAppInitialized();
    }

    public function getSupportedCountries()
    {
        $countries = ConfigFacade::get('api_calendarific.supported_countries');

        $countries = collect($countries)
            ->map(function ($country, $countryCode) {
                $entry = [
                    'country' => $country,
                    'code' => $countryCode,
                ];

                return $entry;
            })
            ->values();

        return $countries;
    }

    public function update(array $data)
    {
        $configData = $this->configTransformer->update($data);

        $now = Carbon::now();
        $defaultTimezone = ConfigFacade::get('app.timezone');

        DB::beginTransaction();

        try
        {
            //default timezone based on the selected country
            $timezoneIdentifiersByCountryCode = DateTimeZone::listIdentifiers
            (DateTimeZone::PER_COUNTRY,  $configData['country']);
            if (empty($timezoneIdentifiersByCountryCode))
            {
                $timezoneIdentifiersByCountryCode = $defaultTimezone;
            }
            else
            {
                $timezoneIdentifiersByCountryCode = $timezoneIdentifiersByCountryCode[0];
            }
            date_default_timezone_set($timezoneIdentifiersByCountryCode);

            //token lifetime
            [$tokenLifetimeValue, $tokenLifetimeUnits] = explode(' ', $configData['token_lifetime']);
            $tokenLifetime = $now
                ->copy()
                ->add($tokenLifetimeValue, $tokenLifetimeUnits);
            Passport::tokensExpireIn($tokenLifetime);

            foreach ($configData as $option => $value)
            {
                $this->configModel->where('option', $option)->update(
                    [
                        'value' => $value,
                    ]
                );
            }

            DB::commit();

            return true;
        }
        catch (\Exception $e)
        {
            //default api timezone
            date_default_timezone_set($defaultTimezone);

            //default api token lifetime
            [$tokenLifetimeValue, $tokenLifetimeUnits] = explode(' ', $this->defaultConfig[1]['value']);
            $tokenLifetime = $now
                ->copy()
                ->add($tokenLifetimeValue, $tokenLifetimeUnits);
            Passport::tokensExpireIn($tokenLifetime);

            DB::rollBack();

            return false;
        }
    }
}