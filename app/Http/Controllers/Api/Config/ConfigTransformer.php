<?php


namespace Api\Config;

use Illuminate\Support\Facades\Config as ConfigFacade;


class ConfigTransformer
{
    /**
     * @var array
     */
    protected $defaultConfig;

    public function __construct()
    {
        $this->defaultConfig = ConfigFacade::get('api_config');
    }

    public function update(array $data)
    {
        $response = [];

        if (isset($data['token_lifetime']))
        {
            $response['token_lifetime'] = $data['token_lifetime'];
        }
        else
        {
            $response['token_lifetime'] = $this->defaultConfig[1]['value'];
        }

        if (isset($data['country']))
        {
            $response['country'] = $data['country'];
        }
        else
        {
            $response['country'] = $this->defaultConfig[2]['value'];
        }

        $response['initialized'] = 'true';

        return $response;
    }
}