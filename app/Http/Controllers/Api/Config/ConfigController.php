<?php


namespace Api\Config;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ConfigController extends Controller
{
    /**
     * @var ConfigService
     */
    protected $configService;

    public function __construct(ConfigService $configService)
    {
        $this->configService = $configService;
    }

    public function get(Request $request)
    {
        $query = $request->query();
        $config = $this->configService->get($query);

        return response()->json(
            $config,
            Response::HTTP_OK,
            []
        );
    }

    public function getDefault()
    {
        $config = $this->configService->getDefault();

        return response()->json(
            $config,
            Response::HTTP_OK,
            []
        );
    }

    public function isInitialized()
    {
        $isAppInitialized = $this->configService->isInitialized();

        $response = [
            'status' => $isAppInitialized,
        ];
        $headers = [];

        if ($isAppInitialized === false)
        {
            $response['error'] = 'true';
            $response['message'] = 'Please contact the Super Admin.';

            $headers['X-Status-Reason'] = 'The App Has Not Been Initialized!';
        }

        return response()->json(
            $response,
            Response::HTTP_OK,
            $headers
        );
    }

    public function getSupportedCountries()
    {
        $supportedCountries = $this->configService->getSupportedCountries();

        return response()->json(
            [
                'supportedCountries' => $supportedCountries,
            ],
            Response::HTTP_OK,
            []
        );
    }

    public function update(Request $request)
    {
        $data = $request->all();

        $status = $this->configService->update($data);

        if ($status === true)
        {
            return response()->json(
                [],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Config Updated Successfully',
                ]
            );
        }
        else if ($status === false)
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Update Config',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Update Config',
                ]
            );
        }
    }
}