<?php


namespace Api\Passport\Client;


use Api\Helpers\Helpers;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Passport\ClientRepository;
use Laravel\Passport\Http\Rules\RedirectRule;
use Laravel\Passport\Http\Controllers\ClientController as PassportClientController;
use Api\Passport\Client\Validator as RequestValidator;
use Laravel\Passport\Passport;

class ClientController extends PassportClientController
{

    /**
     * API Client Service (repository) to be used for custom operations
     *
     * @var ClientService
     */
    protected $clientService;

    /**
     * Create a client controller instance.
     *
     * @param ClientRepository $clients
     * @param ValidationFactory $validation
     * @param RedirectRule $redirectRule
     * @param ClientService $clientService
     *
     * @return void
     */
    public function __construct(
        ClientRepository $clients,
        ValidationFactory $validation,
        RedirectRule $redirectRule,
        ClientService $clientService
    ) {
        parent::__construct($clients, $validation, $redirectRule);

        $this->clientService = $clientService;
    }

    /**
     * Get client based on redirect url and rights assigned
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function findOrFail(Request $request) : JsonResponse
    {
        $data = $request->all();

        $baseUrl = Helpers::url()->getApiBaseUrl($request);
        $data['baseUrl'] = $baseUrl;

        $client = $this->clientService->findOrFail($data);

        return response()->json(
            [
                'client' => $client
            ],
            Response::HTTP_OK,
            [
                'X-Status-Reason' => 'FindOrFail: Returned client or null',
            ]
        );
    }

    /**
     * Store a new client.
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws BindingResolutionException
     */
    public function store(Request $request) : JsonResponse
    {
        $validator = new RequestValidator($request);

        if ($validator->failed())
        {
            $errors = $validator->errors();
            return response()->json(
                $errors['data'],
                $errors['status'],
                $errors['headers']
            );
        }

        $rules = $validator->rules();
        $filledRequest = $this->clientService->fillRequestNullFields($request, $rules);
        $data = $filledRequest->all();

        $client = $this->clients
            ->create(
                $data['user_id'],
                $data['name'],
                $data['redirect'],
                $data['personal_access_client'],
                $data['password_client'],
                $data['confidential']
            )
            ->makeVisible('secret');

        return response()->json(
            [
                'client' => $client,
            ],
            Response::HTTP_OK,
            [
                'X-Status-Reason' => 'Client Stored Successfully',
            ]
        );
    }
}
