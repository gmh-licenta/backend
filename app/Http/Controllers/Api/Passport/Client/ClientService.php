<?php


namespace Api\Passport\Client;


use Api\Helpers\Helpers;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Laravel\Passport\Client;
use Laravel\Passport\ClientRepository;
use Laravel\Passport\Passport;

class ClientService
{
    /**
     * Array containing all clients default values grouped by request method
     *
     * @var array
     */
    protected $defaults;

    protected $clientToApiHiddenFields = [
        'password_client',
        'personal_access_client',
        'revoked',
        'user_id',
        'created_at',
        'updated_at',
        'redirect',
    ];

    /**
     * Client
     *
     * @var ClientRepository
     */
    protected $clientModel;

    public function __construct()
    {
        $this->clientModel = Passport::client();
        $this->defaults = config('defaults.passport.clients');
    }

    /**
     * Tries to return the client. If the client doesn't exist, returns null
     *
     * @param array $data
     *
     * @return Client|null
     */
    public function findOrFail(array $data) : ?Client
    {
        try {
            return $this->clientModel
                ->where('name', $data['name'])
                ->where('user_id', $data['user_id'])
                ->where('personal_access_client', $data['personal_access_client'])
                ->where('password_client', $data['password_client'])
                ->where(function ($query) use ($data) {
                    $query->orWhere('redirect', $data['redirect'])
                        ->orWhere('redirect', $data['baseUrl'])
                        ;
                })
                ->where('redirect', $data['redirect'])
                ->where('revoked', 0)
                ->firstOrFail()
                ->makeHidden($this->clientToApiHiddenFields)
                ;
        }
        catch (ModelNotFoundException $e)
        {
            return null;
        }
    }

    /**
     * Fill in nullable params if not provided in the request.
     *
     * @param Request $request
     * @param array $rules
     *
     * @return Request
     */
    public function fillRequestNullFields(Request $request, array $rules) : Request
    {
        $defaults = $this->getDefaultsByRequestMethod($request);

        foreach ($rules as $field => $fieldRules)
        {
            if (in_array('nullable', $fieldRules))
            {
                if (!$request->has($field) && array_key_exists($field, $defaults))
                {
                    $request->request->set($field, $defaults[$field]);
                }
            }
        }

        if ($request->has('personal_access_client') && !$request->has('password_client'))
        {
            if ($request->request->get('personal_access_client') === false)
            {
                $request->request->set('password_client', false);
            }
            else if ($request->request->get('personal_access_client') === true)
            {
                $request->request->set('password_client', false);
            }
        }
        else if ($request->has('password_client') && !$request->has('personal_access_client'))
        {
            if ($request->request->get('password_client') === false)
            {
                $request->request->set('personal_access_client', false);
            }
            else if ($request->request->get('password_client') === true)
            {
                $request->request->set('personal_access_client', false);
            }
        }

        return $request;
    }

    /**
     * Get default values for request params by request method
     *
     * @param Request $request
     *
     * @return array
     */
    public function getDefaultsByRequestMethod(Request $request) : array
    {
        $response = [];

        switch ($request->getMethod())
        {
            case 'POST':
                $response = $this->defaults['store'];
                break;
            default:
                break;
        }

        return $response;
    }

    /**
     * Parse boolean values from strings (html encoding) to booleans
     *
     * @param Request $request
     * @param array $rules
     *
     * @return Request
     */
    public function parseBooleans(Request $request, array $rules) : Request
    {
        foreach ($rules as $field => $fieldRules)
        {
            if (in_array('boolean', $fieldRules))
            {
                if ($request->has($field))
                {
                    $value = $request->request->get($field);
                    $request->request->set($field, filter_var($value, FILTER_VALIDATE_BOOLEAN));
                }
            }
        }

        return $request;
    }
}
