<?php


namespace Api\Passport\Client;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Passport\Http\Rules\RedirectRule;
use Illuminate\Contracts\Validation\Factory;
use Laravel\Passport\Passport;
use Illuminate\Contracts\Validation\Validator as IValidator;
use Illuminate\Support\Facades\Validator as FValidator;

class Validator
{
    /**
     * Bool to track if the validation failed
     *
     * @var bool
     */
    protected $failed = false;

    /**
     * Array containing data, status and headers to be returned for a json response
     *
     * @var array
     */
    protected $errorResponse = [];

    /**
     * Redirect Rule used in Laravel Passport
     *
     * @var RedirectRule
     */
    protected $redirectRule;

    /**
     * The request to be validated
     *
     * @var Request
     */
    protected $request;

    /**
     * API Client Service (repository) to be used for custom operations
     *
     * @var ClientService
     */
    protected $clientService;

    /**
     * Validator constructor.
     *
     * @param Request $request
     *
     * @return void
     *
     * @throws BindingResolutionException
     */
    public function __construct(Request $request)
    {
        $factory = app()->make(Factory::class);
        $this->redirectRule = new RedirectRule($factory);

        $this->request = $request;
        $this->clientService = new ClientService();

        $this->validate();
    }

    /**
     * Returns the request validation status
     *
     * @return bool
     */
    public function failed() : bool
    {
        return $this->failed;
    }

    /**
     * Returns the request validation encountered errors as an array
     *
     * @return array
     */
    public function errors() : array
    {
        return $this->errorResponse;
    }

    /**
     * Run the validator
     *
     * @return void
     */
    public function validate() : void
    {
        $this->request = $this->clientService->parseBooleans($this->request, $this->rules());
        $data = $this->request->all();

        $validator = $this->validator($data);

        if ($validator->fails())
        {
            $this->failed = true;
            $this->errorResponse = [
                'data' => [
                    'message' => 'Field Validation Failed.',
                    'errors' => $validator->errors()->toArray(),
                ],
                'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'headers' => [
                    'X-Status-Reason' => 'Field Validation Failed',
                ]
            ];
            return;
        }

        $this->request = $this->clientService->fillRequestNullFields($this->request, $this->rules());
        $data = $this->request->all();
        $this->validateCustomRules($data);

        return;
    }

    /**
     * Get validation rules based on the request method
     *
     * @return array
     */
    public function rules() : array
    {
        $rules = [];

        switch ($this->request->method())
        {
            case 'POST':
                $rules = [
                    'name' => ['required', 'max:255'],
                    'user_id' => ['nullable', 'integer'],
                    'confidential' => ['nullable', 'boolean'],
                    'personal_access_client' => ['nullable', 'boolean'],
                    'password_client' => ['nullable', 'boolean'],
                    'redirect' => ['required', $this->redirectRule],
                ];
                break;
            default:
                break;
        }

        return $rules;
    }

    /**
     * Get a validator for an incoming client create request.
     *
     * @param  array  $data
     *
     * @return IValidator
     */
    public function validator(array $data) : IValidator
    {
        return FValidator::make($data, $this->rules());
    }

    public function validateCustomRules($data) : void
    {
        $customRules = $this->customRules();

        foreach ($customRules as $group => $customRule)
        {
            $this->{$customRule}($data);

            if ($this->failed === true)
            {
                return;
            }
        }
    }

    public function customRules()
    {
        return [
            'clientRightsValidation',
            'uniqueValidation',
            'redirectUniqueValidation'
        ];
    }

    /**
     * Check if provided client rights match the client model requirements.
     *
     * @param $data
     *
     * @return void
     */
    public function clientRightsValidation($data) : void
    {
        if (isset($data['personal_access_client']) && isset($data['password_client']))
        {
            if ($data['personal_access_client'] === false && $data['password_client'] === false)
            {
                $this->failed = true;
                $this->errorResponse = [
                    'data' => [
                        'message' => 'Field Validation Failed.',
                        'errors' => [
                            'personal_access_client' => [
                                'The client must either be able to generate personal access tokens or password granted tokens',
                            ],
                            'password_client' => [
                                'The client must either be able to generate personal access tokens or password granted tokens',
                            ],
                        ],
                    ],
                    'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'headers' => [
                        'X-Status-Reason' => 'Field Validation Failed',
                    ]
                ];
                return;
            }
            else if ($data['personal_access_client'] === true && $data['password_client'] === true)
            {
                $this->failed = true;
                $this->errorResponse = [
                    'data' => [
                        'message' => 'Field Validation Failed.',
                        'errors' => [
                            'personal_access_client' => [
                                'The client cannot be eligible to generate both personal access tokens and password granted tokens',
                            ],
                            'password_client' => [
                                'The client cannot be eligible to generate both personal access tokens and password granted tokens',
                            ],
                        ],
                    ],
                    'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'headers' => [
                        'X-Status-Reason' => 'Field Validation Failed',
                    ]
                ];
                return;
            }
        }
        else if (!isset($data['personal_access_client']) && !isset($data['password_client']))
        {
            $this->failed = true;
            $this->errorResponse = [
                'data' => [
                    'message' => 'Field Validation Failed.',
                    'errors' => [
                        'personal_access_client' => [
                            'Both fields personal_access_client and password_client are missing. At least one is required.',
                        ],
                        'password_client' => [
                            'Both fields personal_access_client and password_client are missing. At least one is required.',
                        ],
                    ],
                ],
                'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'headers' => [
                    'X-Status-Reason' => 'Field Validation Failed',
                ]
            ];
            return;
        }
    }

    /**
     * Validate if client unique combination already exists:
     * user_id
     * name
     * personal_access_client
     * password_client
     * redirect
     *
     * @param $data array
     *
     * @return void
     */
    public function uniqueValidation($data) : void
    {
        $rule = Passport::client()
            ->where('name', $data['name'])
            ->where('redirect', $data['redirect'])
            ->where('personal_access_client', $data['personal_access_client'])
            ->where('password_client', $data['password_client'])
            ->first();

        if ($rule !== null)
        {
            $this->failed = true;
            $this->errorResponse = [
                'data' => [
                    'message' => 'A client with the same rights, name and redirect associated to the same user already exists!',
                    'errors' => [
                        'name' => [
                            'The name is already in use.',
                        ],
                        'redirect' => [
                            'A client with the same name and rights is already using this redirect.',
                        ],
                        'personal_access_client' => [
                            'A client with the same name and redirect already exists with this right.',
                        ],
                        'password_client' => [
                            'A client with the same name and redirect already exists with this right.',
                        ],
                        'user_id' => [
                            'A client with the same name, redirect and rights is already associated with this user.',
                        ]
                    ],
                ],
                'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'headers' => [
                    'X-Status-Reason' => 'Field Validation Failed',
                ]
            ];
            return;
        }
    }

    /**
     * Check if there already exists a client with same rights for the particular redirect.
     * Unique combination
     * user_id
     * personal_access_client
     * password_client
     * redirect
     *
     * @param $data array
     *
     * @return void
     */
    public function redirectUniqueValidation($data) : void
    {
        $rule = Passport::client()
            ->where('user_id', $data['user_id'])
            ->where('redirect', $data['redirect'])
            ->where('personal_access_client', $data['personal_access_client'])
            ->where('password_client', $data['password_client'])
            ->first();

        if ($rule !== null)
        {
            $this->failed = true;
            $this->errorResponse = [
                'data' => [
                    'message' => 'A client with the same rights, associated with the same user and redirect already exists!',
                    'errors' => [
                        'redirect' => [
                            'A client with the same rights is already using this redirect.',
                        ],
                        'personal_access_client' => [
                            'A client with the same name and redirect already exists with this right.',
                        ],
                        'password_client' => [
                            'A client with the same name and redirect already exists with this right.',
                        ],
                        'user_id' => [
                            'A client with the same name, redirect and rights is already associated with this user.',
                        ]
                    ],
                ],
                'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'headers' => [
                    'X-Status-Reason' => 'Field Validation Failed',
                ]
            ];
            return;
        }
    }
}
