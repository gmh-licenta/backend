<?php


namespace Api\Passport\Auth;


use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LogoutController
{
    public function index(Request $request)
    {
        $data = $request->all();

        $token = Auth::user()
            ->tokens
            ->where('client_id', $data['clientId'])
            ->first();

        if ($token === null)
        {
            return response()->json(
                [
                    'message' => 'A token for the current user provided by this client could not be found'
                ],
                Response::HTTP_NOT_FOUND,
                [
                    'X-Status-Reason' => 'Entity not found',
                ]
            );
        }

        $token->update([
            'revoked' => 1,
        ]);
//        $token->delete();
        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $token->id)
            ->update([
                'revoked' => 1,
            ]);

        return response()->json(
            'User logged out successfully!',
            Response::HTTP_RESET_CONTENT,
            [
                'X-Status-Reason' => 'User logged out',
            ]
        );
    }
}