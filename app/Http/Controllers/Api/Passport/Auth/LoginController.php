<?php


namespace Api\Passport\Auth;


use Api\Config\Config as ApiConfig;
use Api\Passport\Client\ClientService;
use Api\UserProfile\UserProfileService;
use Facade\FlareClient\Api;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Str;
use Laravel\Passport\Http\Controllers\AccessTokenController;

use Laravel\Passport\Passport;
use Laravel\Passport\TokenRepository;
use League\OAuth2\Server\AuthorizationServer;
use Psr\Http\Message\ServerRequestInterface;
use Lcobucci\JWT\Parser as JwtParser;

class LoginController extends AccessTokenController
{

    /**
     * Client service to access the model
     *
     * @var ClientService
     */
    protected $clientService;

    /**
     * @var UserProfileService
     */
    protected $userProfileService;

    public function __construct(AuthorizationServer $server,
        TokenRepository $tokens,
        JwtParser $jwt,
        ClientService $clientService,
        UserProfileService $userProfileService
    )
    {
        parent::__construct($server, $tokens, $jwt);

        $this->clientService = $clientService;
        $this->userProfileService = $userProfileService;
    }

    /**
     * @param ServerRequestInterface $request
     * @return JsonResponse
     */
    public function index(ServerRequestInterface $request) : JsonResponse
    {
        $data = $request->getParsedBody();
        unset($data['client']);

        $validator = $this->validator($data);

        if ($validator->fails())
        {
            return response()->json(
                [
                    'message' => 'Credentials could not be validated.',
                    'errors' => $validator->errors(),
                ],
                Response::HTTP_UNPROCESSABLE_ENTITY,
                [
                    'X-Status-Reason' => 'Credentials Validation Failed',
                ]
            );
        }

        if (!Auth::attempt($data))
        {
            return response()->json(
                [
                    'message' => "Credentials could not be matched in the database.",
                ],
                Response::HTTP_UNAUTHORIZED,
                [
                    'X-Status-Reason' => 'Invalid Credentials',
                ]
            );
        }

        $loggedInUser = Auth::user();

        //issue access token
        $issueTokenResponse = $this->issueAPIPasswordGrantToken($request);
        $parsedContent = json_decode($issueTokenResponse->getContent());

        $loggedInUserRoles = $loggedInUser->roles;

        $loggedInUserPermissions = collect();
        foreach($loggedInUserRoles as $role)
        {
            $loggedInUserPermissions = $loggedInUserPermissions->merge($role->permissions);
        }
        $loggedInUserPermissions = $loggedInUserPermissions
            ->groupBy(function ($permission, $key) {
                if (Str::startsWith($permission->name, 'view.'))
                {
                    return 'view';
                }
                else
                {
                    return 'action';
                }
            });
        if(!$loggedInUserPermissions->has('action'))
        {
            $loggedInUserPermissions->put('action', collect());
        }
        if(!$loggedInUserPermissions->has('view'))
        {
            $loggedInUserPermissions->put('view', collect());
        }

        $loggedInUserRoles = $loggedInUserRoles
            ->map(function ($role, $key) {
                $role = $role->name;

                return $role;
            })
        ;
        $loggedInUserPermissions = $loggedInUserPermissions
            ->map(function ($permissions, $type) {
                $permissions = $permissions->map(function ($permission, $key) {
                    $permission = $permission->name;

                    return $permission;
                });

                return $permissions;
            })
        ;

        $apiConfig = ApiConfig::getMappedConfig();

        $profile = $this->userProfileService->getForCurrentUser();

        return response()->json(
            [
                'user' => $loggedInUser,
                'tokens' => $parsedContent,
                'roles' => $loggedInUserRoles,
                'permissions' => $loggedInUserPermissions,
                'profile' => $profile,
                'apiConfig' => $apiConfig,
            ],
            Response::HTTP_OK,
            [
                'X-Status-Reason' => 'Authentication Successful',
            ]
        );
    }

    /**
     * Get a validator for an incoming login request.
     *
     * @param  array  $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data) : \Illuminate\Contracts\Validation\Validator
    {
        return Validator::make($data, [
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return Response
     */
    public function issueAPIPasswordGrantToken(ServerRequestInterface $request)
    {
        $parsedBody = $request->getParsedBody();

        $client = $this->getClient($parsedBody['client']);

        $parsedBody['username'] = $parsedBody['email'];
        $parsedBody['grant_type'] = 'password';
        $parsedBody['client_id'] = $client->id;
        $parsedBody['client_secret'] = $client->secret;

        // since it is not required by passport
        unset($parsedBody['email'], $parsedBody['client_name'], $parsedBody['client']);

        return parent::issueToken($request->withParsedBody($parsedBody));
    }

    private function getClient(array $data)
    {
        return Passport::client()
            ->where([
                ['id', $data['id']],
                ['name', $data['name']],
                ['revoked', 0]
            ])
            ->first();
    }
}
