<?php


namespace Api\Passport\Auth;


use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use Laravel\Passport\Passport;
use Psr\Http\Message\ServerRequestInterface;

class TokenController extends AccessTokenController
{

    public function checkToken(ServerRequestInterface $request)
    {
//        $data = $request->all();
        $data = $request->getParsedBody();

        if (!isset($data['clientId']))
        {
            return response()->json(
                [
                    'message' => "Parameter 'clientId' could not be found."
                ],
                Response::HTTP_UNAUTHORIZED,
                [
                    'X-Status-Reason' => 'Invalid Or Missing Parameters!',
                ]
            );
        }

        if (!isset($data['userId']))
        {
            return response()->json(
                [
                    'message' => "Parameter 'userId' could not be found."
                ],
                Response::HTTP_UNAUTHORIZED,
                [
                    'X-Status-Reason' => 'Invalid Or Missing Parameters!',
                ]
            );
        }

        $loggedInUser = Auth::user();

        if ($loggedInUser === null)
        {
            DB::table('oauth_access_tokens')
                ->where('user_id', $data['userId'])
                ->where('client_id', $data['clientId'])
                ->update([
                    'revoked' => 1,
                ]);
            return response()->json(
                [
                    'message' => "Please Login Again"
                ],
                Response::HTTP_UNAUTHORIZED,
                [
                    'X-Status-Reason' => 'Session Expired',
                ]
            );
        }

        $token = $loggedInUser->tokens
            ->where('client_id', $data['clientId'])
            ->first();


        if ($token === null)
        {
            DB::table('oauth_access_tokens')
                ->where('user_id', $data['userId'])
                ->where('client_id', $data['clientId'])
                ->update([
                    'revoked' => 1,
                ]);
            return response()->json(
                [
                    'message' => "Please Login Again"
                ],
                Response::HTTP_UNAUTHORIZED,
                [
                    'X-Status-Reason' => 'Session Expired',
                ]
            );
        }

        if ($token->expires_at->lte($token->created_at))
        {
            DB::table('oauth_access_tokens')
                ->where('user_id', $data['userId'])
                ->where('client_id', $data['clientId'])
                ->update([
                    'revoked' => 1,
                ]);
//            $token->revoke();
            return response()->json(
                [
                    'message' => "Please Login Again"
                ],
                Response::HTTP_UNAUTHORIZED,
                [
                    'X-Status-Reason' => 'Session Expired',
                ]
            );
        }

        if ($token->revoked === 1)
        {
            DB::table('oauth_access_tokens')
                ->where('user_id', $data['userId'])
                ->where('client_id', $data['clientId'])
                ->update([
                    'revoked' => 1,
                ]);
//            $token->revoke();
            return response()->json(
                [
                    'message' => "Please Login Again"
                ],
                Response::HTTP_UNAUTHORIZED,
                [
                    'X-Status-Reason' => 'Session Expired',
                ]
            );
        }

        return response()->json(
            [
                'message' => 'The token is valid'
            ],
            Response::HTTP_NO_CONTENT,
            [
                'X-Status-Reason' => 'Valid Token',
            ]
        );
    }

    public function refreshToken(ServerRequestInterface $request)
    {
        $tokenAlive = $this->checkToken($request);

        //if token ain't alive for whatever reason, return the check response
        if ($tokenAlive->status() !== Response::HTTP_NO_CONTENT)
        {
            return $tokenAlive;
        }

        $issueTokenResponse = $this->issueAPIPasswordGrantToken($request);
        $parsedContent = json_decode($issueTokenResponse->getContent());

        return response()->json(
            [
                'tokens' => $parsedContent,
            ],
            Response::HTTP_OK,
            []
        );
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return Response
     */
    public function issueAPIPasswordGrantToken(ServerRequestInterface $request)
    {
        $parsedBody = $request->getParsedBody();

        $client = $this->getClient($parsedBody['clientId']);

        $tokenRequestBody = [
            'grant_type' => 'refresh_token',
            'refresh_token' => $parsedBody['refreshToken'],
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'scope' => '',
        ];

        return parent::issueToken($request->withParsedBody($tokenRequestBody));
    }

    private function getClient($clientId)
    {
        return Passport::client()
            ->where([
                ['id', $clientId],
                ['revoked', 0]
            ])
            ->first();
    }
}