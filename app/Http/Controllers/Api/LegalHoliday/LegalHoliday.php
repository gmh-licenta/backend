<?php


namespace Api\LegalHoliday;

use Illuminate\Database\Eloquent\Model;

class LegalHoliday extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'legal_holidays';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that will be instanced using Carbon on interaction.
     *
     * @var array
     */
    protected $dates = [
        'date',
    ];

    /**
     * Holiday Types
     */
    const HOLIDAY_TYPE_PUBLIC = "Public";
    const HOLIDAY_TYPE_OBSERVANCE = "Observance";

    /**
     * Holiday Defaults
     */
    const HOLIDAY_DEFAULT_NAME = "Unknown";

    public function getHolidaysByCountry(string $country)
    {
        return self::where('country_code', $country)->get();
    }
}