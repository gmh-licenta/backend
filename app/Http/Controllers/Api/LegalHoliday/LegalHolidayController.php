<?php


namespace Api\LegalHoliday;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LegalHolidayController extends Controller
{

    /**
     * @var LegalHolidayService
     */
    protected $legalHolidayService;

    public function __construct(LegalHolidayService $legalHolidaysService)
    {
        $this->legalHolidayService = $legalHolidaysService;
    }

    public function get(Request $request)
    {
        $query = $request->query();

        $legalHolidays = $this->legalHolidayService->get($query);

        return response()->json(
            [
                'legalHolidays' => $legalHolidays,
            ],
            Response::HTTP_OK
        );
    }

    public function initializeDefault()
    {
        $legalHolidays = $this->legalHolidayService->init();

        return response()->json(
            [
                'legalHolidays' => $legalHolidays,
            ],
            Response::HTTP_OK
        );
    }

    public function create(Request $request)
    {
        $data = $request->all();

        $legalHoliday = $this->legalHolidayService->create($data);

        if ($legalHoliday instanceof LegalHoliday)
        {
            return response()->json(
                [
                    'legalHoliday' => $legalHoliday,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Legal Holiday Added Successfully',
                ]
            );
        }
        else if ($legalHoliday === null)
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Add New Legal Holiday',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Add New Legal Holiday',
                ]
            );
        }
    }

    public function update(Request $request)
    {
        $data = $request->all();

        $legalHoliday = $this->legalHolidayService->update($data);

        if ($legalHoliday instanceof LegalHoliday)
        {
            return response()->json(
                [
                    'legalHoliday' => $legalHoliday,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Legal Holiday Updated Successfully',
                ]
            );
        }
        else if ($legalHoliday === null)
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Update Legal Holiday',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Update Legal Holiday',
                ]
            );
        }
    }

    public function delete(Request $request)
    {
        $data = $request->all();

        $status = $this->legalHolidayService->delete($data);

        if ($status === true)
        {
            return response()->json(
                [],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Legal Holiday Deleted Successfully',
                ]
            );
        }
        else if ($status === false)
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Delete Legal Holiday',
                ]
            );
        }
        else if ($status === null)
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Delete Legal Holiday',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Delete Legal Holiday',
                ]
            );
        }
    }
}