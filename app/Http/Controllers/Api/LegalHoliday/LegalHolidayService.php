<?php

namespace Api\LegalHoliday;

use Api\Config\Config as ApiConfig;
use Carbon\Carbon;
use Calendarific\Calendarific;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class LegalHolidayService
{
    /**
     * @var LegalHoliday
     */
    protected $legalHolidayModel;

    /**
     * @var LegalHolidayTransformer
     */
    protected $legalHolidayTransformer;

    /**
     * @var DefaultLegalHoliday
     */
    protected $defaultLegalHolidayModel;

    /**
     * @var array
     */
    protected $calendarificApiConfig;

    /**
     * @var ApiConfig
     */
    protected $configModel;

    /**
     * @var string
     */
    protected $country;

    public function __construct(LegalHoliday $legalHolidayModel, LegalHolidayTransformer
    $legalHolidayTransformer, DefaultLegalHoliday $defaultLegalHolidayModel, ApiConfig $configModel)
    {
        $this->legalHolidayModel = $legalHolidayModel;
        $this->legalHolidayTransformer = $legalHolidayTransformer;

        $this->defaultLegalHolidayModel = $defaultLegalHolidayModel;

        $this->calendarificApiConfig = Config::get('api_calendarific');

        $this->configModel = $configModel;

        $this->country = $this->configModel->where('option', 'country')->first()->value;
    }

    public function init()
    {
        if ($this->defaultLegalHolidayModel::count() === 0)
        {
            $countries = $this->calendarificApiConfig['supported_countries'];

            foreach ($countries as $countryCode => $countryName)
            {
                $calendarificApiLegalHolidays = $this->getLegalHolidaysCalendarificApi($countryCode);

                foreach ($calendarificApiLegalHolidays as $calendarificApiLegalHoliday)
                {
                    $transformedDefaultLegalHoliday = $this->legalHolidayTransformer
                        ->transformCalendarificToDefault($calendarificApiLegalHoliday);

                    $this->defaultLegalHolidayModel->create($transformedDefaultLegalHoliday);

                    $transformedAppLegalHoliday = $this->legalHolidayTransformer
                        ->transformCalendarificToApp($calendarificApiLegalHoliday);

                    $this->legalHolidayModel->create($transformedAppLegalHoliday);
                }
            }
        }

        $legalHolidays = $this->legalHolidayModel->all();

        $legalHolidays = $legalHolidays->map(function ($legalHoliday, $key) {
            $legalHoliday->dateFormatted = $legalHoliday->date->copy()->format('F d, Y');
            return $legalHoliday;
        })
            ->sortBy(function ($legalHoliday, $key) {
                return Carbon::parse($legalHoliday->date)->timestamp;
            });

        $legalHolidays = $legalHolidays
            ->groupBy('country_code')
            ->map(function ($legalHolidays, $countryCode) {
                $legalHolidays = $legalHolidays
                    ->groupBy(function ($legalHoliday, $key) {
                        return $legalHoliday->type;
                    });

                if (!$legalHolidays->has($this->legalHolidayModel::HOLIDAY_TYPE_PUBLIC))
                {
                    $legalHolidays->put($this->legalHolidayModel::HOLIDAY_TYPE_PUBLIC, collect());
                }

                if (!$legalHolidays->has($this->legalHolidayModel::HOLIDAY_TYPE_OBSERVANCE))
                {
                    $legalHolidays->put($this->legalHolidayModel::HOLIDAY_TYPE_OBSERVANCE, collect());
                }

                return $legalHolidays;
            });

        return $legalHolidays;
    }

    public function getLegalHolidaysCalendarificApi($country = null)
    {
        if ($country === null)
        {
            $country = $this->country;
        }

        $now = Carbon::now();

//        $key = config("api_calendarific.config.api_key");
        $key = $this->calendarificApiConfig['config']['api_key'];
        if ($key === null)
        {
            return [];
        }

        //        $country = $this->country;
        $year = $now->year;
        $month = null;
        $day = null;
        $location = null;
        $types = [];

        $holidays = Calendarific::make(
            $key,
            $country,
            $year,
            $month,
            $day,
            $location,
            $types
        );

        if ($holidays['meta']['code'] !== 200)
        {
            $holidays = [];
        }
        else
        {
            $holidays = $holidays['response']['holidays'];
        }

        return $holidays;
    }

    public function get(array $query)
    {
        $legalHolidays = $this->legalHolidayModel;

        if (!array_key_exists('country_code', $query))
        {
            $countryCode = ApiConfig::getCountryCode();
            $legalHolidays = $legalHolidays->where('country_code', $countryCode);
        }

        foreach ($query as $column => $value)
        {
            $legalHolidays = $legalHolidays->where($column, 'like', '%' . $value . '%');
        }

        $legalHolidays = $legalHolidays->get();

        $legalHolidays = $legalHolidays->map(function ($legalHoliday, $key) {
                $legalHoliday->dateFormatted = $legalHoliday->date->copy()->format('F d, Y');
                return $legalHoliday;
            })
            ->sortBy(function ($legalHoliday, $key) {
                return Carbon::parse($legalHoliday->date)->timestamp;
            });

        $desiredOrder = [
            LegalHoliday::HOLIDAY_TYPE_PUBLIC,
            LegalHoliday::HOLIDAY_TYPE_OBSERVANCE,
        ];
        $legalHolidays = $legalHolidays
            ->groupBy(function ($legalHoliday, $key) {
                return $legalHoliday->type;
            })
            ->sortBy(function ($legalHolidays, $type) use ($desiredOrder) {
                return array_search($type, $desiredOrder);
            });

        if (!$legalHolidays->has(LegalHoliday::HOLIDAY_TYPE_PUBLIC))
        {
            $legalHolidays->put(LegalHoliday::HOLIDAY_TYPE_PUBLIC, collect());
        }

        if (!$legalHolidays->has(LegalHoliday::HOLIDAY_TYPE_OBSERVANCE))
        {
            $legalHolidays->put(LegalHoliday::HOLIDAY_TYPE_OBSERVANCE, collect());
        }

        return $legalHolidays;
    }

    public function create(array $data)
    {
        $transformedData = $this->legalHolidayTransformer->create($data);

        DB::beginTransaction();

        try
        {
            $legalHoliday = $this->legalHolidayModel->create($transformedData);

            DB::commit();

            $legalHoliday->dateFormatted = $legalHoliday->date->copy()->format('F d, Y');

            return $legalHoliday;
        }
        catch (\Exception $e)
        {
            DB::rollBack();

            return null;
        }
    }

    public function update(array $data)
    {
        $legalHoliday = $this->legalHolidayModel->find($data['id']);

        $transformedData = $this->legalHolidayTransformer->update($data, $legalHoliday);

        DB::beginTransaction();

        try
        {
            $legalHoliday->update($transformedData);

            DB::commit();

            $legalHoliday->dateFormatted = $legalHoliday->date->copy()->format('F d, Y');

            return $legalHoliday;
        }
        catch (\Exception $e)
        {
            DB::rollBack();

            return null;
        }
    }

    public function delete(array $data)
    {
        DB::beginTransaction();

        try
        {
            $status = $this->legalHolidayModel->find($data['id'])->delete();

            DB::commit();

            return $status;
        }
        catch (\Exception $e)
        {
            DB::rollBack();

            return null;
        }
    }
}