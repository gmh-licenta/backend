<?php


namespace Api\LegalHoliday;


use Carbon\Carbon;
use Illuminate\Support\Facades\Config as ConfigFacade;
use Illuminate\Support\Str;
use Api\Config\Config as ApiConfig;

class LegalHolidayTransformer
{
    /**
     * @var LegalHoliday
     */
    protected $legalHolidayModel;

    /**
     * @var array
     */
    protected $calendarificApiConfig;

    /**
     * @var ApiConfig
     */
    protected $configModel;

    public function __construct(LegalHoliday $legalHolidayModel, ApiConfig $configModel)
    {
        $this->legalHolidayModel = $legalHolidayModel;
        $this->configModel = $configModel;

        $this->calendarificApiConfig = ConfigFacade::get('api_calendarific');
    }

    public function transformCalendarificToDefault(array $holiday)
    {
        $type = $this->parseHolidayApiTypesToAppType($holiday['type']);
        $date = Carbon::create($holiday['date']['datetime']['year'], $holiday['date']['datetime']['month'], $holiday['date']['datetime']['day'])->toDateString();

        $response = [
            'name' => $holiday['name'],
            'country_code' => $holiday['country']['id'],
            'date' => $date,
            'type' => $type,
        ];

        return $response;
    }

    public function transformCalendarificToApp(array $holiday)
    {
        $type = $this->parseHolidayApiTypesToAppType($holiday['type']);
        $date = Carbon::create($holiday['date']['datetime']['year'], $holiday['date']['datetime']['month'], $holiday['date']['datetime']['day'])->toDateString();

        $response = [
            'local_name' => 'Unknown',
            'fallback_name' => $holiday['name'],
            'country_code' => $holiday['country']['id'],
            'date' => $date,
            'type' => $type,
        ];

        return $response;
    }

    /**
     * Parse API returned holiday types to one of the supported app types
     * App types: [
     *      'Public'
     *      'Observance'
     * ]
     *
     * @param array $apiTypes
     *
     * @return string
     */
    public function parseHolidayApiTypesToAppType(array $apiTypes): string
    {
        $appType = null;

        $appPublicTypesMappedToApiTypes = $this->calendarificApiConfig['publicHolidaysAppTypeToApiType'];

        foreach ($apiTypes as $apiType)
        {
            foreach ($appPublicTypesMappedToApiTypes as $mapType)
            {
                if (strpos(Str::lower($apiType), Str::lower($mapType)) !== false)
                {
                    $appType = $this->legalHolidayModel::HOLIDAY_TYPE_PUBLIC;
                }
            }
        }

        if ($appType === null)
        {
            $appType = $this->legalHolidayModel::HOLIDAY_TYPE_OBSERVANCE;
        }

        return $appType;
    }

    public function create(array $data)
    {
        if (!array_key_exists('local_name', $data))
        {
            $data['local_name'] = $this->legalHolidayModel::HOLIDAY_DEFAULT_NAME;
        }
        if (!array_key_exists('fallback_name', $data))
        {
            $data['fallback_name'] = $this->legalHolidayModel::HOLIDAY_DEFAULT_NAME;
        }
        $data['date'] = Carbon::parse($data['date'])->toDateString();
        $data['country_code'] = Str::lower($this->configModel::getCountryCode());

        $response = [
            'local_name' => $data['local_name'],
            'fallback_name' => $data['fallback_name'],
            'country_code' => $data['country_code'],
            'date' => $data['date'],
            'type' => $data['type'],
        ];

        return $response;
    }

    public function update(array $data, LegalHoliday $legalHoliday)
    {
        if (!array_key_exists('local_name', $data))
        {
            $data['local_name'] = $legalHoliday->fallback_name;
        }

        $data['date'] = Carbon::parse($data['date'])->toDateString();

        $response = [
            'local_name' => $data['local_name'],
            'date' => $data['date'],
            'type' => $data['type'],
        ];

        return $response;
    }
}