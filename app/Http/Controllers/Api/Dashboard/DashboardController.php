<?php

namespace Api\Dashboard;

use Api\LegalHoliday\LegalHolidayService;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class DashboardController extends Controller
{
    /**
     * @var LegalHolidayService
     */
    protected $legalHolidayService;

    public function __construct(LegalHolidayService $legalHolidaysService)
    {
        $this->legalHolidayService = $legalHolidaysService;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $legalHolidays = $this->legalHolidayService->getLegalHolidays(true);

        return response()->json(
            [
                'legalHolidays' => $legalHolidays,
            ],
            Response::HTTP_OK
        );
    }
}
