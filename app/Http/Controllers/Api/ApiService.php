<?php

namespace Api;

use Api\LegalHoliday\LegalHolidayService;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Route;

class ApiService
{

    /**
     * @var LegalHolidayService
     */
    public $legalHolidayService;

    public function __construct(LegalHolidayService $legalHolidaysService)
    {
        $this->legalHolidayService = $legalHolidaysService;

//        $this->initializeDefaultRoles();
    }

    public function initializeDefaultRoles()
    {
//        $superAdmin = Role::create([
//            'name' => 'Super Admin',
//            'guard_name' => 'api',
//        ]);
//        $admin = Role::create([
//            'name' => 'Admin',
//            'guard_name' => 'api'
//        ]);
//        $projectManager = Role::create([
//            'name' => 'Project Manager',
//            'guard_name' => 'api'
//        ]);
//        $employee = Role::create([
//            'name' => 'Employee',
//            'guard_name' => 'api'
//        ]);

        $routes = Route::getRoutes();

        foreach ($routes as $route)
        {
            if (Str::startsWith($route->getName(), 'api.'))
            {
                dump($route->getName());
            }
        }

        dd();

//        $permission = Permission::create([
//            'name' => 'clocking.',
//            'guard_name' => 'api',
//        ]);
    }
}