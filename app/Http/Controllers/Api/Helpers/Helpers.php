<?php

namespace Api\Helpers;

class Helpers
{

    /**
     * @var _Url
     */
    protected static $urlHelper = _Url::class;

    /**
     * @var _String
     */
    protected static $stringHelper = _String::class;

    /**
     * @var _Date
     */
    protected static $dateHelper = _Date::class;

    /**
     * @return _Url
     */
    public static function url()
    {
        return new static::$urlHelper;
    }

    /**
     * @return _String
     */
    public static function str()
    {
        return new static::$stringHelper;
    }

    /**
     * @return _Date
     */
    public static function date()
    {
        return new static::$dateHelper;
    }
}