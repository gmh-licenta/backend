<?php


namespace Api\Helpers;


class _String
{
    function parseRoleToDisplay($role)
    {
        $role = preg_split('/(?=[A-Z])/', $role, -1,PREG_SPLIT_NO_EMPTY);
        $role =  implode(' ', $role);

        return $role;
    }
}