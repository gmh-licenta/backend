<?php


namespace Api\Helpers;


use Api\Config\Config as ApiConfig;
use Api\LegalHoliday\LegalHoliday;
use Carbon\Carbon;

class _Date
{
    public function getNumberOfWorkingDaysInInterval(Carbon $startDate, Carbon $endDate)
    {
        $days = 0;

        //add days without weekends
        $currentDate = $startDate->copy();
        while ($currentDate->lte($endDate))
        {
            if ($currentDate->dayOfWeek !== Carbon::SATURDAY && $currentDate->dayOfWeek !==
                Carbon::SUNDAY)
            {
                $days++;
            }
            $currentDate->addDay();
        }

        $countryCode = ApiConfig::getCountryCode();

        $legalHolidaysInInterval = LegalHoliday::where('country_code', $countryCode)
            ->where('type', LegalHoliday::HOLIDAY_TYPE_PUBLIC)
            ->whereBetween('date', [$startDate, $endDate])
            ->get();

        //remove public holidays
        foreach ($legalHolidaysInInterval as $legalHoliday)
        {
            if ($legalHoliday->date->dayOfWeek !== Carbon::SATURDAY &&
                $legalHoliday->date->dayOfWeek !== Carbon::SUNDAY)
            {
                $days--;
            }
        }

        return $days;
    }
}