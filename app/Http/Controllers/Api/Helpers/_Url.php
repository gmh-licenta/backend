<?php

namespace Api\Helpers;


use Illuminate\Http\Request;

class _Url
{

    /**
     * Accepted url's for localhost connection (only for development purposes)
     *
     * @var array
     */
    private $localhost = [
        'http://127.0.0.1:8000',
        'http://127.0.0.1:8080',
        'http://localhost:8000',
        'http://localhost:8080',
        'http://192.168.0.108:8000',
        'http://192.168.0.108:8080',
//        'http://::1:8000',
//        'http://::1:8080',
    ];
    /**
     * Returns the sender's base url the request has been sent from.
     *
     * @param Request $request
     *
     * @return string
     */
    public function getApiBaseUrl(Request $request) : string
    {
//        if (filter_has_var(INPUT_SERVER, 'REMOTE_ADDR')) {
//            $ip = filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_VALIDATE_IP);
//        } else if (filter_has_var(INPUT_ENV, 'REMOTE_ADDR')) {
//            $ip = filter_input(INPUT_ENV, 'REMOTE_ADDR', FILTER_VALIDATE_IP);
//        } else if (isset($_SERVER['REMOTE_ADDR'])) {
//            $ip = filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP);
//        } else {
//            $ip = null;
//        }

        $fullUrl = $request->server('HTTP_REFERER');
        $urlSegments = parse_url($fullUrl);

        $baseUrl = $urlSegments['scheme'] . '://' . $urlSegments['host'];
        if (array_key_exists('port', $urlSegments))
        {
            $baseUrl .= ':' . $urlSegments['port'];
        }

        //this is only for development purposes
        if (in_array($baseUrl, $this->localhost))
        {
            $baseUrl = 'http://gmhfrontend.develop.eiddew.com';
        }

        return $baseUrl;
    }
}