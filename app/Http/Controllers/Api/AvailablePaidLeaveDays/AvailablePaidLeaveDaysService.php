<?php


namespace Api\AvailablePaidLeaveDays;


use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class AvailablePaidLeaveDaysService
{
    /**
     * @var AvailablePaidLeaveDays
     */
    protected $availablePaidLeaveDaysModel;

    /**
     * @var AvailablePaidLeaveDaysTransformer
     */
    protected $availablePaidLeaveDaysTransformer;

    /**
     * @var User
     */
    protected $userModel;

    /**
     * @var User
     */
    protected $defaultSuperAdminId;

    public function __construct(AvailablePaidLeaveDays $availablePaidLeaveDaysModel,
        AvailablePaidLeaveDaysTransformer $availablePaidLeaveDaysTransformer, User $userModel)
    {
        $this->availablePaidLeaveDaysModel = $availablePaidLeaveDaysModel;
        $this->availablePaidLeaveDaysTransformer = $availablePaidLeaveDaysTransformer;

        $this->userModel = $userModel;

        $defaults = Config::get('defaults');
        $this->defaultSuperAdminId = $defaults['users']['super_admin_id'];
    }

    public function get(array $query)
    {
        $loggedInUser = Auth::user();

        $availablePaidLeaveDays = $this->availablePaidLeaveDaysModel;

        //single user
        if (array_key_exists('user_id', $query))
        {
            $availablePaidLeaveDays = $availablePaidLeaveDays
                ->where('user_id', $query['user_id']);
        }
        //if id != superAdminId => employee, get only self records
        else if ($loggedInUser->id !== $this->defaultSuperAdminId)
        {
            $availablePaidLeaveDays = $availablePaidLeaveDays->where('user_id', $loggedInUser->id);
        }

        foreach ($query as $column => $value)
        {
            $availablePaidLeaveDays = $availablePaidLeaveDays->where($column, 'like', '%' . $value . '%');
        }

        $availablePaidLeaveDays = $availablePaidLeaveDays->get();

        $availablePaidLeaveDays = $availablePaidLeaveDays
            ->map(function ($entry, $key) {
                $entry->user = $entry->user;
                $entry->expires_at_display = $entry->expires_at->toDateString();

                return $entry;
            });

        $employees = $this->userModel
            ->where('id', '!=', $this->defaultSuperAdminId)
            ->get();

        $employees = $employees
            ->map(function ($employee, $key) {
                $mappedEmployee = [
                    'id' => $employee->id,
                    'name' => $employee->name,
                    'email' => $employee->email,
                ];

                return $mappedEmployee;
            });

        $response = [
            'availablePaidLeaveDays' => $availablePaidLeaveDays,
            'employees' => $employees,
        ];

        return $response;
    }

    public function create(array $data)
    {
        $transformedData = $this->availablePaidLeaveDaysTransformer->create($data);

        DB::beginTransaction();

        try
        {
            $availablePaidLeaveDays = $this->availablePaidLeaveDaysModel->create($transformedData);

            DB::commit();

            $availablePaidLeaveDays->user = $availablePaidLeaveDays->user;

            return $availablePaidLeaveDays;
        }
        catch (\Exception $e)
        {
            DB::rollBack();

            return null;
        }
    }

    public function update(array $data)
    {
        $entry = $this->availablePaidLeaveDaysModel->find($data['id']);

        $transformedData = $this->availablePaidLeaveDaysTransformer->update($data);

        DB::beginTransaction();

        try
        {
            $entry->update($transformedData);

            DB::commit();

            $entry->user = $entry->user;

            return $entry;
        }
        catch (\Exception $e)
        {
            DB::rollBack();

            return null;
        }
    }
}