<?php


namespace Api\AvailablePaidLeaveDays;


use Carbon\Carbon;

class AvailablePaidLeaveDaysTransformer
{
    public function create(array $data)
    {
        $response = [
            'user_id' => $data['user_id'],
            'year' => $data['year'],
            'days' => $data['days'],
            'expires_at' => Carbon::parse($data['expires_at'])->toDateString(),
        ];

        return $response;
    }

    public function update(array $data)
    {
        $response = [
            'user_id' => $data['user_id'],
            'year' => $data['year'],
            'days' => $data['days'],
            'expires_at' => Carbon::parse($data['expires_at'])->toDateString(),
        ];

        return $response;
    }
}