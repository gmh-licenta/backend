<?php

namespace Api\AvailablePaidLeaveDays;

use Api\LegalHoliday\LegalHoliday;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AvailablePaidLeaveDaysController extends Controller
{
    /**
     * @var AvailablePaidLeaveDaysService
     */
    protected $availablePaidLeaveDaysService;

    public function __construct(AvailablePaidLeaveDaysService $availablePaidLeaveDaysService)
    {
        $this->availablePaidLeaveDaysService = $availablePaidLeaveDaysService;
    }

    public function get(Request $request)
    {
        $query = $request->query();

        $response = $this->availablePaidLeaveDaysService->get($query);

        return response()->json(
            $response,
            Response::HTTP_OK,
            []
        );
    }

    public function create(Request $request)
    {
        $data = $request->all();

        $entry = $this->availablePaidLeaveDaysService->create($data);

        if ($entry instanceof AvailablePaidLeaveDays)
        {
            return response()->json(
                [
                    'entry' => $entry,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Available Paid Leave Days Entry Added Successfully',
                ]
            );
        }
        else if ($entry === null)
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Add New Available Paid Leave Days Entry',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Add New Available Paid Leave Days Entry',
                ]
            );
        }
    }

    public function update(Request $request)
    {
        $data = $request->all();

        $entry = $this->availablePaidLeaveDaysService->update($data);

        if ($entry instanceof AvailablePaidLeaveDays)
        {
            return response()->json(
                [
                    'entry' => $entry,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Available Paid Leave Days Entry Updated Successfully',
                ]
            );
        }
        else if ($entry === null)
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Update Available Paid Leave Days Entry',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Update Available Paid Leave Days Entry',
                ]
            );
        }
    }
}