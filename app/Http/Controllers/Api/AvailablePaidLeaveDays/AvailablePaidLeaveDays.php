<?php


namespace Api\AvailablePaidLeaveDays;


use App\User;
use Illuminate\Database\Eloquent\Model;

class AvailablePaidLeaveDays extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'available_paid_leave_days';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that will be instanced using Carbon on interaction.
     *
     * @var array
     */
    protected $dates = [
        'expires_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}