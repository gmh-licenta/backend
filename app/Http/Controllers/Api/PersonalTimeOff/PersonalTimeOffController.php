<?php

namespace Api\PersonalTimeOff;


use Api\Clocking\Clocking;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PersonalTimeOffController
{

    /**
     * @var PersonalTimeOffService
     */
    protected $personalTimeOffService;

    public function __construct(PersonalTimeOffService $personalTimeOffService)
    {
        $this->personalTimeOffService = $personalTimeOffService;
    }

    public function create(Request $request)
    {
        $data = $request->all();

        $paidLeaveRequest = $this->personalTimeOffService->create($data);

        if ($paidLeaveRequest instanceof PaidLeave)
        {
            return response()->json(
                [],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Paid Leave Request Registered Successfully',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Paid Leave Request Failed To Be Registered',
                ]
            );
        }

    }

    public function create(Request $request)
    {
        $data = $request->all();

        $sickLeaveRequest = $this->personalTimeOffService->create($data);

        if ($sickLeaveRequest instanceof SickLeave)
        {
            return response()->json(
                [],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Sick Leave Request Registered Successfully',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Sick Leave Request Failed To Be Registered',
                ]
            );
        }

    }

    public function getForCurrentUser()
    {
        $paidLeaves = $this->personalTimeOffService->getForCurrentUser();

        return response()->json(
            [
                'paidLeaves' => $paidLeaves
            ],
            Response::HTTP_OK,
            [
                'X-Status-Reason' => 'Paid Leaves For User',
            ]
        );
    }

    public function getForCurrentUser()
    {
        $sickLeaves = $this->personalTimeOffService->getForCurrentUser();

        return response()->json(
            [
                'sickLeaves' => $sickLeaves
            ],
            Response::HTTP_OK,
            [
                'X-Status-Reason' => 'Sick Leaves For User',
            ]
        );
    }
}