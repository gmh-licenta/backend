<?php


namespace Api\PersonalTimeOff;


use Illuminate\Support\Facades\Auth;

class PersonalTimeOffService
{

    /**
     * @var PaidLeave
     */
    protected $paidLeaveModel;

    /**
     * @var SickLeave
     */
    protected $sickLeaveModel;

    /**
     * @var PersonalTimeOffTransformer
     */
    protected $personalTimeOffTransformer;

    public function __construct(PaidLeave $paidLeaveModel, SickLeave $sickLeaveModel,
        PersonalTimeOffTransformer $personalTimeOffTransformer)
    {
        $this->paidLeaveModel = $paidLeaveModel;
        $this->sickLeaveModel = $sickLeaveModel;

        $this->personalTimeOffTransformer = $personalTimeOffTransformer;
    }

    public function create(array $data)
    {
        $loggedInUser = Auth::user();
        $data['user_id'] = $loggedInUser->id;

        $data = $this->personalTimeOffTransformer->paidLeave($data);

        $paidLeaveRequest = $this->paidLeaveModel->create($data);

        return $paidLeaveRequest;
    }

    public function create(array $data)
    {
        $loggedInUser = Auth::user();
        $data['user_id'] = $loggedInUser->id;

        $data = $this->personalTimeOffTransformer->sickLeave($data);

        $sickLeaveRequest = $this->sickLeaveModel->create($data);

        return $sickLeaveRequest;
    }

    public function getForCurrentUser()
    {
        $loggedInUser = Auth::user();

        $paidLeaves = $this->paidLeaveModel
            ->where('user_id', $loggedInUser->id)
            ->get()
            ->map(function ($paidLeave, $key) {
                $paidLeave->start_date_display = $paidLeave->start_date->copy()->format('Y-m-d');
                $paidLeave->end_date_display = $paidLeave->end_date->copy()->format('Y-m-d');
                return $paidLeave;
            })
            ->groupBy('status')
            ->map(function ($paidLeavesByStatus, $status) {
                $paidLeavesByStatus = $paidLeavesByStatus
                    ->sortByDesc('start_date')
                    ->values()
                ;
                return $paidLeavesByStatus;
            });

        if (!$paidLeaves->has(PaidLeave::STATUS_UNPROCESSED))
        {
            $paidLeaves->put(PaidLeave::STATUS_UNPROCESSED, []);
        }
        if (!$paidLeaves->has(PaidLeave::STATUS_APPROVED))
        {
            $paidLeaves->put(PaidLeave::STATUS_APPROVED, []);
        }
        if (!$paidLeaves->has(PaidLeave::STATUS_REJECTED))
        {
            $paidLeaves->put(PaidLeave::STATUS_REJECTED, []);
        }

        return $paidLeaves;
    }

    public function getForCurrentUser()
    {
        $loggedInUser = Auth::user();

        $sickLeaves = $this->sickLeaveModel
            ->where('user_id', $loggedInUser->id)
            ->get()
            ->map(function ($sickLeave, $key) {
                $sickLeave->start_date_display = $sickLeave->start_date->copy()->format('Y-m-d');
                $sickLeave->end_date_display = $sickLeave->end_date->copy()->format('Y-m-d');
                return $sickLeave;
            })
            ->groupBy('status')
            ->map(function ($sickLeavesByStatus, $status) {
                $sickLeavesByStatus = $sickLeavesByStatus
                    ->sortByDesc('start_date')
                    ->values()
                ;
                return $sickLeavesByStatus;
            });

        if (!$sickLeaves->has(PaidLeave::STATUS_UNPROCESSED))
        {
            $sickLeaves->put(PaidLeave::STATUS_UNPROCESSED, []);
        }
        if (!$sickLeaves->has(PaidLeave::STATUS_APPROVED))
        {
            $sickLeaves->put(PaidLeave::STATUS_APPROVED, []);
        }
        if (!$sickLeaves->has(PaidLeave::STATUS_REJECTED))
        {
            $sickLeaves->put(PaidLeave::STATUS_REJECTED, []);
        }

        return $sickLeaves;
    }
}