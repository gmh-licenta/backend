<?php


namespace Api\PersonalTimeOff;


use Carbon\Carbon;

class PersonalTimeOffTransformer
{

    public function paidLeave(array $data): array
    {
        $startDate = Carbon::parse($data['startDate'])->startOfDay();
        $endDate = Carbon::parse($data['endDate'])->endOfDay();

        $data['start_date'] = $startDate;
        $data['end_date'] = $endDate;
        $data['days'] = $endDate->diffInDays($startDate) + 1;
        $data['status'] = PaidLeave::STATUS_UNPROCESSED;
        $data['status_date'] = Carbon::now();

        unset($data['startDate']);
        unset($data['endDate']);

        return $data;
    }

    public function sickLeave(array $data): array
    {
        $startDate = Carbon::parse($data['startDate'])->startOfDay();
        $endDate = Carbon::parse($data['endDate'])->endOfDay();

        $data['start_date'] = $startDate;
        $data['end_date'] = $endDate;
        $data['days'] = $endDate->diffInDays($startDate) + 1;
        $data['status'] = SickLeave::STATUS_UNPROCESSED;
        $data['status_date'] = Carbon::now();

        unset($data['startDate']);
        unset($data['endDate']);

        return $data;
    }
}