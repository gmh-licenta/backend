<?php


namespace Api\PersonalTimeOff\PaidLeave;


use Api\AvailablePaidLeaveDays\AvailablePaidLeaveDays;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class PaidLeaveService
{

    /**
     * @var PaidLeave
     */
    protected $paidLeaveModel;

    /**
     * @var PaidLeaveTransformer
     */
    protected $paidLeaveTransformer;

    /**
     * @var AvailablePaidLeaveDays
     */
    protected $availablePaidLeaveDaysModel;

    /**
     * @var string
     */
    protected $defaultSuperAdminId;

    public function __construct(PaidLeave $paidLeaveModel,
        PaidLeaveTransformer $paidLeaveTransformer, AvailablePaidLeaveDays $availablePaidLeaveDaysModel)
    {
        $this->paidLeaveModel = $paidLeaveModel;
        $this->paidLeaveTransformer = $paidLeaveTransformer;

        $this->availablePaidLeaveDaysModel = $availablePaidLeaveDaysModel;


        $defaults = Config::get('defaults');
        $this->defaultSuperAdminId = $defaults['users']['super_admin_id'];
    }

    public function create(array $data)
    {
        $loggedInUser = Auth::user();
        $data['user_id'] = $loggedInUser->id;

        $data = $this->paidLeaveTransformer->paidLeave($data);

        $paidLeaveRequest = $this->paidLeaveModel->create($data);

        return $paidLeaveRequest;
    }

    public function get(array $query)
    {
        $loggedInUser = Auth::user();
        $adminRole = Role::where('name', 'Admin')->first();

        $paidLeaves = $this->paidLeaveModel;

        //single user
        if (array_key_exists('user_id', $query))
        {
            $paidLeaves = $paidLeaves->where('user_id', $query['user_id']);
        }
        //if admin, get all without the ones for other admins including self
        else if ($loggedInUser->roles->first()->name === $adminRole->name)
        {
            $admins = DB::table('model_has_roles')
                    ->where('role_id', $adminRole->id)
                    ->pluck('model_id');

            $paidLeaves = $paidLeaves->whereNotIn('user_id', $admins);
        }
        //if not admin && id != superAdminId => employee, get only self records
        else if ($loggedInUser->id !== $this->defaultSuperAdminId)
        {
            $paidLeaves = $paidLeaves->where('user_id', $loggedInUser->id);
        }

        if (array_key_exists('date', $query))
        {
            $paidLeaves = $paidLeaves->where('start_date', '<=', $query['date'])
                ->where('end_date', '>=', $query['date']);

            unset($query['date']);
        }

        foreach ($query as $column => $value)
        {
            $paidLeaves = $paidLeaves->where($column, 'like', '%' . $value . '%');
        }

        $paidLeaves = $paidLeaves->get();

        $paidLeaves = $paidLeaves
            ->map(function ($paidLeave, $key) {
                $paidLeave->user = $paidLeave->user;
                $paidLeave->user_changed_status = $paidLeave->userChangedStatus;

                $paidLeave->start_date_display = $paidLeave->start_date->copy()->format('Y-m-d');
                $paidLeave->end_date_display = $paidLeave->end_date->copy()->format('Y-m-d');

                return $paidLeave;
            })
            ->groupBy('status')
            ->map(function ($paidLeavesByStatus, $status) {
                $paidLeavesByStatus = $paidLeavesByStatus
                    ->sortByDesc('start_date')
                    ->values()
                ;

                return $paidLeavesByStatus;
            });

        if (!$paidLeaves->has(PaidLeave::STATUS_UNPROCESSED))
        {
            $paidLeaves->put(PaidLeave::STATUS_UNPROCESSED, []);
        }
        if (!$paidLeaves->has(PaidLeave::STATUS_APPROVED))
        {
            $paidLeaves->put(PaidLeave::STATUS_APPROVED, []);
        }
        if (!$paidLeaves->has(PaidLeave::STATUS_REJECTED))
        {
            $paidLeaves->put(PaidLeave::STATUS_REJECTED, []);
        }

        return $paidLeaves;
    }

    public function approve(array $data)
    {
        $now = Carbon::now();
        $loggedInUser = Auth::user();

        if (!array_key_exists('id', $data))
        {
            return [
                'error' => true,
                'message' => "Missing Parameter 'id'",
            ];
        }
        $paidLeave = $this->paidLeaveModel->find($data['id']);

        DB::beginTransaction();

        try
        {
            $paidLeave->update([
                'status' => PaidLeave::STATUS_APPROVED,
                'status_changed_by_user_id' => $loggedInUser->id,
                'status_date' => Carbon::now()->toDateTimeString(),
            ]);

            $paidLeaveDays = $paidLeave->days;

            $availablePaidLeaveDays = $this->availablePaidLeaveDaysModel
                ->where('user_id', $paidLeave->user_id)
                ->where('days', '!=', 0)
                ->where('expires_at', '>=', $now->toDateString())
                ->get()
                ->sortBy('year');

            foreach ($availablePaidLeaveDays as $entry)
            {
                if ($paidLeaveDays === 0)
                {
                    break;
                }

                if ($paidLeaveDays > $entry->days)
                {
                    $paidLeaveDays -= $entry->days;

                    $entry->update([
                        'days' => 0,
                    ]);
                }
                else
                {
                    $remainingDays = $entry->days - $paidLeaveDays;

                    $entry->update([
                        'days' => $remainingDays,
                    ]);
                }
            }

            DB::commit();

            return $paidLeave;
        }
        catch (\Exception $e)
        {
            DB::rollBack();

            return [
                'error' => true,
                'message' => 'Failed To Approve Paid Leave',
            ];
        }
    }

    public function reject(array $data)
    {
        $loggedInUser = Auth::user();

        if (!array_key_exists('id', $data))
        {
            return [
                'error' => true,
                'message' => "Missing Parameter 'id'",
            ];
        }
        $paidLeave = $this->paidLeaveModel->find($data['id']);

        DB::beginTransaction();

        try
        {
            $paidLeave->update([
                'status' => PaidLeave::STATUS_REJECTED,
                'status_changed_by_user_id' => $loggedInUser->id,
                'status_date' => Carbon::now()->toDateTimeString(),
            ]);

            DB::commit();

            return $paidLeave;
        }
        catch (\Exception $e)
        {
            DB::rollBack();

            return [
                'error' => true,
                'message' => 'Failed To Reject Paid Leave',
            ];
        }
    }
}