<?php


namespace Api\PersonalTimeOff\PaidLeave;


use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaidLeave extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'paid_leaves';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that will be instanced using Carbon on interaction.
     *
     * @var array
     */
    protected $dates = [
        'start_date',
        'end_date',
        'status_date',
    ];

    public const STATUS_APPROVED = 'Approved';
    public const STATUS_REJECTED = 'Rejected';
    public const STATUS_UNPROCESSED = 'Unprocessed';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function userChangedStatus()
    {
        return $this->belongsTo(User::class, 'status_changed_by_user_id', 'id');
    }
}