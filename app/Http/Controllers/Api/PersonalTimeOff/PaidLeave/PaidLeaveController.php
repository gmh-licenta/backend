<?php


namespace Api\PersonalTimeOff\PaidLeave;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PaidLeaveController extends Controller
{

    /**
     * @var PaidLeaveService
     */
    protected $paidLeaveService;

    public function __construct(PaidLeaveService $paidLeaveService)
    {
        $this->paidLeaveService = $paidLeaveService;
    }

    public function create(Request $request)
    {
        $data = $request->all();

        $paidLeaveRequest = $this->paidLeaveService->create($data);

        if ($paidLeaveRequest instanceof PaidLeave)
        {
            return response()->json(
                [],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Paid Leave Request Registered Successfully',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Paid Leave Request Failed To Be Registered',
                ]
            );
        }

    }

    public function get(Request $request)
    {
        $query = $request->query();
        $paidLeaves = $this->paidLeaveService->get($query);

        return response()->json(
            [
                'paidLeaves' => $paidLeaves
            ],
            Response::HTTP_OK,
            [
                'X-Status-Reason' => 'Paid Leaves For User',
            ]
        );
    }

    public function approve(Request $request)
    {
        $data = $request->all();
        $response = $this->paidLeaveService->approve($data);

        if ($response instanceof PaidLeave)
        {
            return response()->json(
                [
                    'paidLeave' => $response,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Paid Leave Approved Successfully',
                ]
            );
        }
        else if (array_key_exists($response['error']) && $response['error'] === true)
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => $response['message'],
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Approve Paid Leave',
                ]
            );
        }
    }
    public function reject(Request $request)
    {
        $data = $request->all();
        $response = $this->paidLeaveService->reject($data);

        if ($response instanceof PaidLeave)
        {
            return response()->json(
                [
                    'paidLeave' => $response,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Paid Leave Rejected Successfully',
                ]
            );
        }
        else if (array_key_exists($response['error']) && $response['error'] === true)
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => $response['message'],
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Approve Paid Leave',
                ]
            );
        }
    }
}