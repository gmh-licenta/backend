<?php


namespace Api\PersonalTimeOff\SickLeave;


use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class SickLeaveService
{

    /**
     * @var SickLeave
     */
    protected $sickLeaveModel;

    /**
     * @var SickLeaveTransformer
     */
    protected $sickLeaveTransformer;

    /**
     * @var string
     */
    protected $defaultSuperAdminId;

    public function __construct(SickLeave $sickLeaveModel,
        SickLeaveTransformer $sickLeaveTransformer)
    {
        $this->sickLeaveModel = $sickLeaveModel;
        $this->sickLeaveTransformer = $sickLeaveTransformer;

        $defaults = Config::get('defaults');
        $this->defaultSuperAdminId = $defaults['users']['super_admin_id'];
    }

    public function create(array $data)
    {
        $loggedInUser = Auth::user();
        $data['user_id'] = $loggedInUser->id;

        $data = $this->sickLeaveTransformer->sickLeave($data);

        $sickLeaveRequest = $this->sickLeaveModel->create($data);

        return $sickLeaveRequest;
    }

    public function get(array $query)
    {
        $loggedInUser = Auth::user();
        $adminRole = Role::where('name', 'Admin')->first();

        $sickLeaves = $this->sickLeaveModel;

        //single user
        if (array_key_exists('user_id', $query))
        {
            $sickLeaves = $sickLeaves->where('user_id', $query['user_id']);
        }
        //if admin, get all without the ones for other admins including self
        else if ($loggedInUser->roles->first()->name === $adminRole->name)
        {
            $admins = DB::table('model_has_roles')
                ->where('role_id', $adminRole->id)
                ->pluck('model_id');

            $sickLeaves = $sickLeaves->whereNotIn('user_id', $admins);
        }
        //if not admin && id != superAdminId => employee, get only self records
        else if ($loggedInUser->id !== $this->defaultSuperAdminId)
        {
            $sickLeaves = $sickLeaves->where('user_id', $loggedInUser->id);
        }

        if (array_key_exists('date', $query))
        {
            $sickLeaves = $sickLeaves->where('start_date', '>=', $query['date'])
                ->where('end_date', '<=', $query['date']);

            unset($query['date']);
        }

        foreach ($query as $column => $value)
        {
            $sickLeaves = $sickLeaves->where($column, 'like', '%' . $value . '%');
        }

        $sickLeaves = $sickLeaves->get();

        $sickLeaves = $sickLeaves
            ->map(function ($sickLeave, $key) {
                $sickLeave->user = $sickLeave->user;
                $sickLeave->user_changed_status = $sickLeave->userChangedStatus;

                $sickLeave->start_date_display = $sickLeave->start_date->copy()->format('Y-m-d');
                $sickLeave->end_date_display = $sickLeave->end_date->copy()->format('Y-m-d');

                return $sickLeave;
            })
            ->groupBy('status')
            ->map(function ($sickLeavesByStatus, $status) {
                $sickLeavesByStatus = $sickLeavesByStatus
                    ->sortByDesc('start_date')
                    ->values()
                ;
                return $sickLeavesByStatus;
            });

        if (!$sickLeaves->has(SickLeave::STATUS_UNPROCESSED))
        {
            $sickLeaves->put(SickLeave::STATUS_UNPROCESSED, []);
        }
        if (!$sickLeaves->has(SickLeave::STATUS_APPROVED))
        {
            $sickLeaves->put(SickLeave::STATUS_APPROVED, []);
        }
        if (!$sickLeaves->has(SickLeave::STATUS_REJECTED))
        {
            $sickLeaves->put(SickLeave::STATUS_REJECTED, []);
        }

        return $sickLeaves;
    }

    public function approve(array $data)
    {
        $loggedInUser = Auth::user();

        if (!array_key_exists('id', $data))
        {
            return [
                'error' => true,
                'message' => "Missing Parameter 'id'",
            ];
        }
        $sickLeave = $this->sickLeaveModel->find($data['id']);

        DB::beginTransaction();

        try
        {
            $sickLeave->update([
                'status' => SickLeave::STATUS_APPROVED,
                'status_changed_by_user_id' => $loggedInUser->id,
                'status_date' => Carbon::now()->toDateTimeString(),
            ]);

            DB::commit();

            return $sickLeave;
        }
        catch (\Exception $e)
        {
            DB::rollBack();

            return [
                'error' => true,
                'message' => 'Failed To Approve Sick Leave',
            ];
        }
    }

    public function reject(array $data)
    {
        $loggedInUser = Auth::user();

        if (!array_key_exists('id', $data))
        {
            return [
                'error' => true,
                'message' => "Missing Parameter 'id'",
            ];
        }
        $sickLeave = $this->sickLeaveModel->find($data['id']);

        DB::beginTransaction();

        try
        {
            $sickLeave->update([
                'status' => SickLeave::STATUS_REJECTED,
                'status_changed_by_user_id' => $loggedInUser->id,
                'status_date' => Carbon::now()->toDateTimeString(),
            ]);

            DB::commit();

            return $sickLeave;
        }
        catch (\Exception $e)
        {
            DB::rollBack();

            return [
                'error' => true,
                'message' => 'Failed To Reject Sick Leave',
            ];
        }
    }
}