<?php

namespace Api\PersonalTimeOff\SickLeave;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SickLeaveController extends Controller
{

    /**
     * @var SickLeaveService
     */
    protected $sickLeaveService;

    public function __construct(SickLeaveService $sickLeaveService)
    {
        $this->sickLeaveService = $sickLeaveService;
    }

    public function create(Request $request)
    {
        $data = $request->all();

        $sickLeaveRequest = $this->sickLeaveService->create($data);

        if ($sickLeaveRequest instanceof SickLeave)
        {
            return response()->json(
                [],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Sick Leave Request Registered Successfully',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Sick Leave Request Failed To Be Registered',
                ]
            );
        }

    }

    public function get(Request $request)
    {
        $query = $request->query();
        $sickLeaves = $this->sickLeaveService->get($query);

        return response()->json(
            [
                'sickLeaves' => $sickLeaves
            ],
            Response::HTTP_OK,
            [
                'X-Status-Reason' => 'Sick Leaves For User',
            ]
        );
    }

    public function approve(Request $request)
    {
        $data = $request->all();
        $response = $this->sickLeaveService->approve($data);

        if ($response instanceof SickLeave)
        {
            return response()->json(
                [
                    'sickLeave' => $response,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Sick Leave Approved Successfully',
                ]
            );
        }
        else if (array_key_exists($response['error']) && $response['error'] === true)
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => $response['message'],
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Approve Sick Leave',
                ]
            );
        }
    }
    public function reject(Request $request)
    {
        $data = $request->all();
        $response = $this->sickLeaveService->reject($data);

        if ($response instanceof SickLeave)
        {
            return response()->json(
                [
                    'sickLeave' => $response,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Sick Leave Rejected Successfully',
                ]
            );
        }
        else if (array_key_exists($response['error']) && $response['error'] === true)
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => $response['message'],
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Approve Sick Leave',
                ]
            );
        }
    }
}