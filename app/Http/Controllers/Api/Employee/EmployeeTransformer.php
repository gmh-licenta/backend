<?php


namespace Api\Employee;


use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;

class EmployeeTransformer
{
    /**
     * @var string
     */
    protected $defaultPassword;

    public function __construct()
    {
        $defaults = Config::get('defaults');
        $this->defaultPassword = $defaults['users']['new_user_password'];
    }

    function create(array $data)
    {
        $userModelData = [
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($this->defaultPassword),
        ];

        $birthday = Carbon::parse($data['birthday'])->toDateTimeString();
        $userProfileData = [
            'department_id' => $data['department_id'],
            'position_id' => $data['position_id'],
            'phone' => $data['phone'],
            'birthday' => $birthday,
        ];

        return [
            'user' => $userModelData,
            'userProfile' => $userProfileData,
        ];
    }

    function update(array $data)
    {
        $loggedInUser = Auth::user();

        $userModelData = [
            'name' => $data['name'],
            'email' => $data['email'],
        ];

        $birthday = Carbon::parse($data['birthday'])->toDateTimeString();
        $userProfileData = [
            'department_id' => $data['department_id'],
            'position_id' => $data['position_id'],
            'phone' => $data['phone'],
            'birthday' => $birthday,
        ];
        if (isset($data['user_id']))
        {
            $userProfileData['user_id'] = $data['user_id'];
        }
        else
        {
            $userProfileData['user_id'] = $loggedInUser->id;
        }

        return [
            'user' => $userModelData,
            'userProfile' => $userProfileData,
        ];
    }
}