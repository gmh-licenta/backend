<?php

namespace Api\Employee;


use Api\Department\DepartmentService;
use Api\Helpers\Helpers;
use Api\Position\PositionService;
use Api\UserProfile\UserProfile;
use Api\UserProfile\UserProfileService;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use ReflectionClass;
use ReflectionProperty;
use Spatie\Permission\Models\Role;

class EmployeeService
{
    /**
     * @var User
     */
    protected $userModel;

    /**
     * @var UserProfileService
     */
    protected $userProfileService;

    /**
     * @var UserProfile
     */
    protected $userProfileModel;

    /**
     * @var PositionService
     */
    protected $positionService;

    /**
     * @var DepartmentService
     */
    protected $departmentService;

    /**
     * @var EmployeeTransformer
     */
    protected $employeeTransformer;

    /**
     * @var int
     */
    protected $defaultSuperAdminId;

    /**
     * @var string
     */
    protected $defaultNewUserRole;

    public function __construct(User $userModel, UserProfileService $userProfileService,
        UserProfile $userProfileModel, DepartmentService $departmentService,
        PositionService $positionService, EmployeeTransformer $employeeTransformer)
    {
        $this->employeeTransformer = $employeeTransformer;

//        $this->userModel = new ReflectionClass(User::class);;
//        $this->userProfileService = new ReflectionClass(UserProfileService::class);;
//        $this->departmentService = new ReflectionClass(DepartmentService::class);;
//        $this->positionService = new ReflectionClass(PositionService::class);;
//
//        $userProfileModelRP= new ReflectionProperty(UserProfileService::class, 'userProfileModel');
//        $userProfileModelRP->setAccessible(true);
//        $this->userProfileModel = $userProfileModelRP->getValue($this->userProfileService);

        $this->userModel = $userModel;
        $this->userProfileService = $userProfileService;
        $this->userProfileModel = $userProfileModel;
        $this->departmentService = $departmentService;
        $this->positionService = $positionService;


        $defaults = Config::get('defaults');
        $this->defaultSuperAdminId = $defaults['users']['super_admin_id'];
        $this->defaultNewUserRole = $defaults['users']['new_user_role'];
    }


    public function getAll()
    {
        $loggedInUser = Auth::user();
        $employees = $this->userModel
//            ->where('id', '!=', $this->defaultSuperAdminId)
            ->whereNotIn('id', [$this->defaultSuperAdminId, $loggedInUser->id])
            ->withTrashed()
            ->get();

        $employees = $employees
            ->map(function ($employee, $key) {
                $employee->role = $employee->roles->first();
                $employee->role->name = Helpers::str()->parseRoleToDisplay($employee->role->name);

                if ($employee->profile === null)
                {
                    $employee->profile = $employee->profile()->withTrashed()->first();
                }

                if ($employee->profile->picture !== null)
                {
                    $employee->profile->picture = $this->userProfileService->convertImageToBase64
                    ($employee->profile->picture);
                }

                $employee->department = $employee->profile->department;
                $employee->position = $employee->profile->position;
                $employee->joined_date = $employee->created_at->format('Y-m-d');
                $employee->birthday = $employee->profile->birthday->format('Y-m-d');

                return $employee;
            })
            ->groupBy(function ($employee, $key) {
                if ($employee->deleted_at !== null)
                {
                    return 'deleted';
                }

                return 'active';
            });

        if (!$employees->has('active'))
        {
            $employees->put('active', collect());
        }
        if (!$employees->has('deleted'))
        {
            $employees->put('deleted', collect());
        }

        return $employees;
    }

    public function create(array $data)
    {
        $transformedData = $this->employeeTransformer->create($data);

        $userData = $transformedData['user'];
        $userProfileData = $transformedData['userProfile'];

        $user = $this->userModel->create($userData);
        if (!($user instanceof User))
        {
            return [
                'error' => true,
                'message' => 'Failed To Create User',
            ];
        }

        $userProfileData['user_id'] = $user->id;
        $userProfile = $this->userProfileService->createProfile($userProfileData);
        if (!($userProfile instanceof UserProfile))
        {
            $user->forceDelete();

            return [
                'error' => true,
                'message' => 'Failed To Create User Profile',
            ];
        }

        try
        {
            $loggedInUser = Auth::user();

            if ($loggedInUser->hasRole('SuperAdmin'))
            {
                if (array_key_exists('role_id', $data))
                {
                    $role = Role::where('id', $data['role_id'])->first();
                    $assignUserRole = $user->assignRole($role);
                }
                else
                {
                    $assignUserRole = $user->assignRole('Employee');
                }
            }
            else
            {
                $assignUserRole = $user->assignRole('Employee');
            }
        }
        catch (\Exception $e)
        {
            $user->forceDelete();
            $userProfile->forceDelete();

            return [
                'error' => true,
                'message' => 'Failed To Assign User Role',
            ];
        }

        $user->role = $user->roles->first();
        $user->role->name = Helpers::str()->parseRoleToDisplay($user->role->name);
        $user->department = $user->profile->department;
        $user->position = $user->profile->position;
        $user->joined_date = $user->created_at->format('Y-m-d');
        $user->birthday = $user->profile->birthday->format('Y-m-d');

        return $user;
    }

    public function update(array $data)
    {
        $loggedInUser = Auth::user();

        $transformedData = $this->employeeTransformer->update($data);

        $userData = $transformedData['user'];
        $userProfileData = $transformedData['userProfile'];

        $user = $this->userModel->find($data['user_id']);
        $userProfile = $this->userProfileModel
            ->where('user_id', $data['user_id'])
            ->first();

        if ($data['user_id'] == $loggedInUser->id)
        {
            if ($data['picture'] !== null)
            {
                $filename = $this->userProfileService->storePicture($data['picture'], $userProfile);
            }
            else
            {
                if ($userProfile->picture !== null)
                {
                    Storage::disk('profile_pictures')->delete($userProfile->picture);
                }

                $filename = null;
            }

            $userProfileData['picture'] = $filename;
        }

        DB::beginTransaction();

        try
        {
            $status = $user->update($userData);

            if ($status === false)
            {
                DB::rollBack();

                return [
                    'error' => true,
                    'message' => 'Failed To Update User',
                ];
            }

            $status = $this->userProfileService
                ->find($user->id)
                ->update($userProfileData);

            if ($status === false)
            {
                DB::rollBack();

                return [
                    'error' => true,
                    'message' => 'Failed To Update User Profile',
                ];
            }

            DB::commit();

        }
        catch (\Exception $e)
        {
            DB::rollBack();

            return [
                'error' => true,
                'message' => 'Failed To Update User Profile (Internal Error)',
            ];
        }

        return true;
    }

    public function delete(array $data)
    {
        DB::beginTransaction();

        try
        {
            $status = $this->userModel->find($data['id'])->delete();

            if ($status === false)
            {
                DB::rollBack();

                return [
                    'error' => true,
                    'message' => 'Failed To Delete User',
                ];
            }

            $status = $this->userProfileModel
                ->where('user_id', $data['id'])
                ->delete();

            if ($status === false)
            {
                DB::rollBack();

                return [
                    'error' => true,
                    'message' => 'Failed To Delete User Profile',
                ];
            }

            DB::commit();

        }
        catch (\Exception $e)
        {
            DB::rollBack();

            return [
                'error' => true,
                'message' => 'Failed To Delete User Profile (Internal Error)',
            ];
        }

        return true;
    }

    public function restore(array $data)
    {
        DB::beginTransaction();

        try
        {
            $user = $this->userModel
                ->withTrashed()
                ->find($data['id']);

            $status = $user->restore();

            if ($status === false)
            {
                DB::rollBack();

                return [
                    'status' => false,
                    'employee' => null,
                ];
            }

            $userProfile = $this->userProfileModel
                ->withTrashed()
                ->where('user_id', ($data['id']))
                ->first();

            $status = $userProfile->restore();

            if ($status === false)
            {
                DB::rollBack();

                return [
                    'status' => false,
                    'employee' => null,
                ];
            }

            DB::commit();

            $employee = $this->userModel->find($data['id']);

            $employee->role = $employee->roles->first();
            $employee->role->name = Helpers::str()->parseRoleToDisplay($employee->role->name);
            $employee->department = $employee->profile->department;
            $employee->position = $employee->profile->position;
            $employee->joined_date = $employee->created_at->format('Y-m-d');
            $employee->birthday = $employee->profile->birthday->format('Y-m-d');

            return [
                'status' => $status,
                'employee' => $employee,
            ];
        }
        catch (\Exception $e)
        {
            DB::rollBack();

            return [
                'status' => false,
                'employee' => null,
            ];
        }

    }
}