<?php

namespace Api\Employee;


use Api\Department\DepartmentService;
use Api\Department\Hierarchy\DepartmentsHierarchy;
use Api\Department\Hierarchy\DepartmentsHierarchyService;
use Api\Position\Hierarchy\PositionsHierarchy;
use Api\Position\Hierarchy\PositionsHierarchyService;
use Api\Position\PositionService;
use Api\UserProfile\UserProfileService;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;
use Spatie\Permission\Models\Role;

class EmployeeController extends Controller
{
    /**
     * @var EmployeeService
     */
    protected $employeeService;

    /**
     * @var UserProfileService
     */
    protected $userProfileService;

    /**
     * @var PositionService
     */
    protected $positionService;

    /**
     * @var DepartmentService
     */
    protected $departmentService;

    /**
     * @var DepartmentsHierarchyService
     */
    protected $departmentsHierarchyService;

    /**
     * @var PositionsHierarchyService
     */
    protected $positionsHierarchyService;

    /**
     * @var int
     */
    protected $defaultSuperAdminRoleId;

    /**
     * @var int
     */
    protected $defaultEmployeeRoleId;

    /**
     * @var int
     */
    protected $defaultDepartmentId;

    /**
     * @var int
     */
    protected $defaultPositionId;

    public function __construct(EmployeeService $employeeService, UserProfileService $userProfileService,
        PositionService $positionService, DepartmentService $departmentService,
        DepartmentsHierarchyService $departmentsHierarchyService, PositionsHierarchyService $positionsHierarchyService)
    {
        $this->employeeService = $employeeService;
        $this->userProfileService = $userProfileService;
        $this->positionService = $positionService;
        $this->departmentService = $departmentService;

        $this->departmentsHierarchyService = $departmentsHierarchyService;
        $this->positionsHierarchyService = $positionsHierarchyService;

        $defaults = Config::get('defaults');
        $this->defaultSuperAdminRoleId = $defaults['roles']['super_admin_id'];
        $this->defaultEmployeeRoleId = $defaults['roles']['employee_id'];
        $this->defaultDepartmentId = $defaults['department']['id'];
        $this->defaultPositionId = $defaults['position']['id'];
    }

    public function getAll()
    {
        $employees = $this->employeeService->getAll();
        $positions = $this->positionService->getAll(true)->get('active');
        $departments = $this->departmentService->getAll(true)->get('active');
        $roles = Role::where('id', '!=', $this->defaultSuperAdminRoleId)
            ->get()
            ->map(function ($role, $key) {
                unset($role->guard_name);
                return $role;
            });

        $defaultRole = Role::find($this->defaultEmployeeRoleId);
        $defaultDepartment = $departments->where('id', $this->defaultDepartmentId)->first();
        $defaultPosition = $positions->where('id', $this->defaultPositionId)->first();


        return response()->json(
            [
                'employees' => [
                    'active' => $employees->get('active'),
                    'deleted' => $employees->get('deleted'),
                ],

                'roles' => $roles,
                'positions' => $positions,
                'departments' => $departments,

                'defaults' => [
                    'role' => $defaultRole,
                    'department' => $defaultDepartment,
                    'position' => $defaultPosition,
                ],
            ],
            Response::HTTP_OK
        );
    }

    public function create(Request $request)
    {
        $data = $request->all();

        $user = $this->employeeService->create($data);

        $this->departmentsHierarchyService->refresh();
        $this->positionsHierarchyService->refresh();

        if ($user instanceof User)
        {
            return response()->json(
                [
                    'employee' => $user,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Employee Added Successfully',
                ]
            );
        }
        else if (isset($user['error']) && $user['error'] === true)
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => $user['message'],
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Add New Employee',
                ]
            );
        }
    }

    public function update(Request $request)
    {
        $data = $request->all();

        $status = $this->userProfileService->update($data);

        $this->departmentsHierarchyService->refresh();
        $this->positionsHierarchyService->refresh();

        if ($status instanceof User)
        {
            return response()->json(
                [
                    'employee' => $status,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Employee Updated Successfully',
                ]
            );
        }
        else if (isset($status['error']) && $status['error'] === true)
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => $status['message'],
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Update Employee',
                ]
            );
        }
    }

    public function delete(Request $request)
    {
        $data = $request->all();

        $status = $this->employeeService->delete($data);

        $this->departmentsHierarchyService->refresh();
        $this->positionsHierarchyService->refresh();

        if ($status === true)
        {
            return response()->json(
                [],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Employee Deleted Successfully',
                ]
            );
        }
        else if (isset($status['error']) && $status['error'] === true)
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => $status['message'],
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Delete Employee',
                ]
            );
        }
    }

    public function restore(Request $request)
    {
        $data = $request->all();

        $response = $this->employeeService->restore($data);

        $this->departmentsHierarchyService->refresh();
        $this->positionsHierarchyService->refresh();

        $status = $response['status'];
        $employee = $response['employee'];

        if ($status === true)
        {
            return response()->json(
                [
                    'employee' => $employee,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Employee Restored Successfully',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Restore Employee',
                ]
            );
        }
    }
}