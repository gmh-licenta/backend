<?php

namespace Api\UserProfile;


use Api\Department\Hierarchy\DepartmentsHierarchy;
use Api\Employee\EmployeeService;
use Api\Employee\EmployeeTransformer;
use Api\Helpers\Helpers;
use Api\Position\Hierarchy\PositionsHierarchy;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use ReflectionClass;
use ReflectionProperty;
use Spatie\Permission\Models\Role;

class UserProfileService
{

    /**
     * @var UserProfile
     */
    protected $userProfileModel;
    /**
     * @var UserProfileTransformer
     */
    protected $userProfileTransformer;

    /**
     * @var PositionsHierarchy
     */
    protected $positionsHierarchyModel;

    /**
     * @var DepartmentsHierarchy
     */
    protected $departmentsHierarchyModel;

    /**
     * @var User
     */
    protected $userModel;

    public function __construct(UserProfile $userProfileModel, User
    $userModel, UserProfileTransformer $userProfileTransformer, PositionsHierarchy
    $positionsHierarchyModel, DepartmentsHierarchy $departmentsHierarchyModel)
    {
        $this->userProfileModel = $userProfileModel;
        $this->userModel = $userModel;

        $this->userProfileTransformer = $userProfileTransformer;

        $this->positionsHierarchyModel = $positionsHierarchyModel;
        $this->departmentsHierarchyModel = $departmentsHierarchyModel;
    }

    public function createProfile(array $data)
    {
        $profile = $this->userProfileModel->create($data);

        return $profile;
    }

    public function update(array $data)
    {
        $transformedData = $this->userProfileTransformer->update($data);
        $userData = $transformedData['user'];
        $userProfileData = $transformedData['userProfile'];

        $user = $this->userModel
            ->where('id', $userProfileData['user_id'])
            ->first();

        $profile = $this->userProfileModel
            ->where('user_id', $userProfileData['user_id'])
            ->first();

        if (array_key_exists('picture', $userProfileData) && isset($userProfileData['picture']))
        {
            if ($userProfileData['picture'] !== null)
            {
                $filename = $this->storePicture($userProfileData['picture'], $profile);
            }
            else
            {
                if ($profile->picture !== null)
                {
                    Storage::disk('profile_pictures')->delete($profile->picture);
                }

                $filename = null;
            }

            $userProfileData['picture'] = $filename;
        }

        DB::beginTransaction();

        try
        {
            if (!empty($userData))
            {
                $status = $user->update($userData);

                if ($status === false)
                {
                    DB::rollBack();

                    return [
                        'error' => true,
                        'message' => 'Failed To Update User',
                    ];
                }
            }

            if (!empty($userProfileData))
            {
                $status = $profile->update($userProfileData);

                if ($status === false)
                {
                    DB::rollBack();

                    return [
                        'error' => true,
                        'message' => 'Failed To Update User Profile',
                    ];
                }
            }

            $loggedInUser = Auth::user();

            if ($loggedInUser->hasRole('SuperAdmin'))
            {
                if (array_key_exists('role_id', $data))
                {
                    $currentRole = $user->roles->first();

                    if ($currentRole->id != $data['role_id'])
                    {
                        $newRole = Role::where('id', $data['role_id'])->first();
                        $user->removeRole($currentRole);
                        $assignUserRole = $user->assignRole($newRole);

                        if ($user->tokens !== null && !$user->tokens->isEmpty())
                        {
                            $tokensIds = $user->tokens
                                ->map(function ($token, $key) {
                                    return $token->id;
                                })
                                ->toArray();

                            DB::table('oauth_access_tokens')
                                ->whereIn('id', $tokensIds)
                                ->update([
                                    'revoked' => 1,
                                ]);

                            DB::table('oauth_refresh_tokens')
                                ->whereIn('access_token_id', $tokensIds)
                                ->update([
                                    'revoked' => 1,
                                ]);
                        }
                    }
                }
            }

            DB::commit();

        }
        catch (\Exception $e)
        {
            DB::rollBack();

            return [
                'error' => true,
                'message' => 'Failed To Update User Profile (Internal Error)',
            ];
        }

        $user = $this->userProfileTransformer->response($user);

        return $user;
    }

    public function storePicture(string $base64Picture, UserProfile $profile)
    {
        $base64Picture = explode(",", $base64Picture);
        $base64Picture = $base64Picture[1];
        $base64Picture = str_replace(' ', '+', $base64Picture);
        $base64Picture = base64_decode($base64Picture);

        $filename = $profile->picture;
        $extension = 'jpg';

        if ($filename === null)
        {
            $filename = $profile->user_id . '_' . Str::random(8) . '.' . $extension;
        }

        Storage::disk('profile_pictures')->put($filename, $base64Picture);

        return $filename;
    }

    public function getForCurrentUser()
    {
        $loggedInUser = Auth::user();

        $profile = $this->userProfileModel
            ->where('user_id', $loggedInUser->id)
            ->first();

        if ($profile->picture !== null)
        {
            $profile->picture = $this->convertImageToBase64($profile->picture);
        }

        return $profile;
    }

    public function convertImageToBase64(string $filename)
    {
        $file = Storage::disk('profile_pictures')->get($filename);

        $storagePath = Config::get('filesystems.disks.profile_pictures.root');
        $fullPath = $storagePath . '/' . $filename;

        $extension = pathinfo($fullPath, PATHINFO_EXTENSION);

        $base64 = 'data:image/' . $extension . ';base64,' . base64_encode($file);

        return $base64;
    }

    public function phoneNumberIsTaken(array $data)
    {
        if (isset($data['phone']))
        {
            if (!isset($data['user_id']))
            {
                $loggedInUser = Auth::user();
                $data['user_id'] = $loggedInUser->id;
            }

            $status = $this->userProfileModel
                ->where('user_id', '!=', $data['user_id'])
                ->where('phone', $data['phone'])
                ->first();

            $status = ($status === null) ? false : true;
        }
        else
        {
            $status = [
                'error' => true,
                'message' => "Parameter 'phone' not found",
            ];
        }

        return $status;
    }
}