<?php

namespace Api\UserProfile;


use Api\Employee\EmployeeService;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class UserProfileController extends Controller
{

    /**
     * @var UserProfileService
     */
    protected $userProfileService;

    /**
     * @var EmployeeService
     */
    protected $employeeService;

    public function __construct(UserProfileService $userProfileService, EmployeeService $employeeService)
    {
        $this->userProfileService = $userProfileService;
        $this->employeeService = $employeeService;
    }

    public function update(Request $request)
    {
        $data = $request->all();

        $status = $this->userProfileService->update($data);

        if ($status instanceof User)
        {
            return response()->json(
                [],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'User Profile Updated Successfully',
                ]
            );
        }
        else if (isset($status['error']) && $status['error'] === true)
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => $status['message'],
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'Failed To Update User Profile',
                ]
            );
        }
    }

    public function getForCurrentUser()
    {
        $profile = $this->userProfileService->getForCurrentUser();

        if ($profile instanceof UserProfile)
        {
            return response()->json(
                [
                    'profile' => $profile,
                    'department' => $profile->department,
                    'position' => $profile->position,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'User Profile Found',
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'User Profile Could Not Be Found',
                ]
            );
        }
    }

    public function phoneNumberIsTaken(Request $request)
    {
        $data = $request->all();

        $status = $this->userProfileService->phoneNumberIsTaken($data);

        if ($status === false)
        {
            return response()->json(
                [
                    'status' => false,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Phone Number Is Available',
                ]
            );
        }
        else if ($status === true)
        {
            return response()->json(
                [
                    'status' => true,
                ],
                Response::HTTP_OK,
                [
                    'X-Status-Reason' => 'Phone Number Is Already Taken',
                ]
            );
        }
        else if (isset($status['error']) && $status['error'] === true)
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => $status['message'],
                ]
            );
        }
        else
        {
            return response()->json(
                [],
                Response::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'X-Status-Reason' => 'User Profile Could Not Be Found',
                ]
            );
        }
    }
}