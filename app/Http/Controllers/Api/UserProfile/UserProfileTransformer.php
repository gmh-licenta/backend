<?php


namespace Api\UserProfile;


use Api\Helpers\Helpers;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;

class UserProfileTransformer
{
    function response(User $user)
    {
        $user->role = $user->roles->first();
        $user->role->name = Helpers::str()->parseRoleToDisplay($user->role->name);
        $user->department = $user->profile->department;
        $user->position = $user->profile->position;
        $user->joined_date = $user->created_at->format('Y-m-d');
        $user->birthday = $user->profile->birthday->format('Y-m-d');

        return $user;
    }

    function update(array $data)
    {
        $loggedInUser = Auth::user();

        $userModelData = [];
        $userProfileData = [];

        if (array_key_exists('phone', $data) && isset($data['phone']))
        {
            $userProfileData['phone'] = $data['phone'];
        }

        if (array_key_exists('birthday', $data) && isset($data['birthday']))
        {
            $birthday = Carbon::parse($data['birthday'])->toDateTimeString();
            $userProfileData['birthday'] = $birthday;
        }

        if (array_key_exists('picture', $data))
        {
            $userProfileData['picture'] = $data['picture'];
        }

        if ($loggedInUser->roles->first()->hasPermissionTo('employee.create') &&
            $loggedInUser->roles->first()->hasPermissionTo('employee.update'))
        {

            if (array_key_exists('department_id', $data) && isset($data['department_id']))
            {
                $userProfileData['department_id'] = $data['department_id'];
            }
            if (array_key_exists('position_id', $data) && isset($data['position_id']))
            {
                $userProfileData['position_id'] = $data['position_id'];
            }

            if (array_key_exists('name', $data) && isset($data['name']))
            {
                $userModelData['name'] = $data['name'];
            }
            if (array_key_exists('email', $data) && isset($data['email']))
            {
                $userModelData['email'] = $data['email'];
            }
        }

        if (array_key_exists('user_id', $data) && isset($data['user_id']))
        {
            $userProfileData['user_id'] = $data['user_id'];
        }
        else
        {
            $userProfileData['user_id'] = $loggedInUser->id;
        }

        if (array_key_exists('password', $data))
        {
            if ($data['password'] === '' || $data['password'] === null)
            {
                $defaults = Config::get('defaults');
                $userModelData['password'] = Hash::make($defaults['users']['new_user_password']);
            }
            else
            {
                $userModelData['password'] = Hash::make($data['password']);
            }
        }

        return [
            'user' => $userModelData,
            'userProfile' => $userProfileData,
        ];

//        $birthday = Carbon::parse($data['birthday'])->toDateTimeString();
//
//        $userProfileData = [
//            'picture' => $data['picture'],
//            'phone' => $data['phone'],
//            'birthday' => $birthday,
//        ];
//
//        $loggedInUser = Auth::user();
//
//        if ($loggedInUser->roles->first()->hasPermissionTo('employee.create') &&
//            $loggedInUser->roles->first()->hasPermissionTo('employee.update'))
//        {
//            $userProfileData['department_id'] = $data['department_id'];
//            $userProfileData['position_id'] = $data['position_id'];
//        }
//
//        return $userProfileData;
    }
}